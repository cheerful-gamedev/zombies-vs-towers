﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;
using UnityEngine.AI;

[RequireComponent(typeof(SwfClip))]
[RequireComponent(typeof(SwfClipController))]
public class TowerInfo : MonoBehaviour
{
    public SceneObject Info; // Ссылка на ScriptableObject с информацией
    public bool hasAnimationOfAttack = false; // Имеет ли башня анимацию атака
    public bool hasAnimationOfTakingDamage = true; // Имеет ли башная анимацию получения урона
    public Transform NavMeshPoint; // Цель для NavMeshAgent'а Зомбо
    [SerializeField] private int numberOfTakingDamageAnimations = 2; // Кол-во анимаций получения урона у Башни
    [SerializeField] private int numberOfDestructionAnimations = 1; // Кол-во анимаций разрушения у Башни

    private SwfClip clip;
    private SwfClipController clipController;
    private SceneObjectSoundsSystem SOSS;

    public void Initialization(SceneObject sceneObject, float damageCoefficient)
    {
        // Create new instance of SceneObject
        sceneObject.Object = gameObject;
        Info = ScriptableObject.CreateInstance<SceneObject>();
        Info.UpdateAllData(sceneObject);
        Info.Damage *= damageCoefficient;

        clip = GetComponent<SwfClip>();
        clipController = GetComponent<SwfClipController>();
        SOSS = GetComponent<SceneObjectSoundsSystem>();

        if(GetComponent<CloseCombat>() != null)
            clipController.rateScale = Info.Speed;
    }

    public bool TakeDamage(float damage)
    {
        Info.CurHealth -= damage;
        if (hasAnimationOfTakingDamage)
        {
            clip.sequence = "tower_" +  Info.ID +  "_takingDamage_"+Random.Range(0, numberOfTakingDamageAnimations);
            clipController.Play(true);
        }

        if (Info.CurHealth <= 0)
        {
            Die();
            return true;
        }
        else
            return false;
    }

    void Die()
    {
        SendMessage("StopAttack"); // Delete attack scripts
        StaticKeeper SK = StaticKeeper.Instance;
        SK.GameDataKeeper.SpawnedTowers.Remove(Info); // Remove from GameDataKeeper list

        try
        {
            List<SceneObject> zombos = SK.GameDataKeeper.SpawnedZombos.FindAll(zombo => zombo.TargetType == TargetType.Tower);
            Transform nextTarget = SK.GameDataKeeper.NearestTowerObject();
            if(nextTarget != null)
            {
                SK.GameDataKeeper.Tower = nextTarget;

                // Refresh targets for all with (TargetType == Tower)
                foreach (SceneObject SO in zombos)
                {
                    SO.TargetType = TargetType.Tower;
                    SO.Object.SendMessage("SetNewTarget", SK.GameDataKeeper.Tower);
                }
            }
            else
            {
                // Refresh targets for all with (TargetType == End)
                foreach (SceneObject SO in zombos)
                {
                    SO.TargetType = TargetType.End;
                    SO.Object.SendMessage("SetNewTarget", SK.GameDataKeeper.End);
                }
            }
        }
        catch { }

        SK.GameDataKeeper.PointsForDestruction += Info.PointForDestruction;
        SK.GameDataKeeper.destroyedTowers++;

        clip.sequence = "tower_" + Info.ID + "_destruction_"+Random.Range(0, numberOfDestructionAnimations);
        clipController.Play(true);

        SOSS.PlayDieSound();

        Destroy(GetComponent<Collider>());
        Destroy(GetComponent<Rigidbody>());
        // Destroy(GetComponent<NavMeshObstacle>());
        //Destroy(gameObject, clip.frameCount/clip.clip.FrameRate);
    }
}
