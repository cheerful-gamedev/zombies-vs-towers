﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;

[RequireComponent(typeof(TowerInfo))]
[RequireComponent(typeof(SwfClipController))]
public class CloseCombat : MonoBehaviour
{
    private TowerInfo TowerInfo;
    private SwfClipController clipController;
    private List<ZomboInfo> zombosInZone = new List<ZomboInfo>(); // ZomboInfo of Zombos in the zone
    private bool attackingNow;

    void Awake()
    {
        TowerInfo = GetComponent<TowerInfo>();
        clipController = GetComponent<SwfClipController>();
    }

    void Attack()
    {
        if(clipController.isStopped && TowerInfo.hasAnimationOfAttack)
                clipController.Play("tower_" + TowerInfo.Info.ID + "_attack_0");

        try{
            foreach (ZomboInfo zi in new List<ZomboInfo>(zombosInZone))
            if (zi.TakeDamage(TowerInfo.Info.Damage)) // If die
                zombosInZone.Remove(zi); // Remove from Tower list
        }
        catch{}
    }

    public void StopAttack()
    {
        Destroy(this);
    }

    // When Zombo entering to the zone
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag.Equals("Zombo"))
        {
            ZomboInfo zi = collider.GetComponent<ZomboInfo>();
            if (zombosInZone.IndexOf(zi) == -1)
            {
                zombosInZone.Add(zi);
                if (!attackingNow)
                {
                    InvokeRepeating("Attack", 0, 2 / TowerInfo.Info.Speed); // Attack every 2 sec
                    attackingNow = true;
                }
            }
        }
    }

    // When Zombo leaving the zone
    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag.Equals("Zombo"))
        {
            ZomboInfo zi = collider.GetComponent<ZomboInfo>();
            zombosInZone.Remove(zi);
            if (zombosInZone.Count == 0)
            {
                CancelInvoke();
                attackingNow = false;
            }
        }
    }
}
