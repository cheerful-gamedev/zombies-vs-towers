﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;

[RequireComponent(typeof(TowerInfo))]
[RequireComponent(typeof(SwfClipController))]
public class Ranger : MonoBehaviour
{
    [SerializeField] private int attackAnimationsNumber = 1;
    [SerializeField] private SpellObject spell;
    private TowerInfo TowerInfo;
    private SwfClipController swfClipController;
    private List<ZomboInfo> zombosInZone = new List<ZomboInfo>(); // ZomboInfo of Zombos in the zone
    private bool attackingNow;

    void Awake()
    {
        TowerInfo = GetComponent<TowerInfo>();
        swfClipController = GetComponent<SwfClipController>();
    }

    void Attack()
    {
        if(swfClipController.isStopped && TowerInfo.hasAnimationOfAttack)
            swfClipController.Play("tower_" + TowerInfo.Info.ID + "_attack_"+Random.Range(0, attackAnimationsNumber));

        try{
            foreach (ZomboInfo zi in new List<ZomboInfo>(zombosInZone))
            {
                zi.Spell.Initialization(spell);
                zombosInZone.Remove(zi); // Remove from Tower list
            }
        }
        catch{}
    }

    public void StopAttack()
    {
        Destroy(this);
    }

    // When Zombo entering to the zone
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag.Equals("Zombo"))
        {
            ZomboInfo zi = collider.GetComponent<ZomboInfo>();
            if (zombosInZone.IndexOf(zi) == -1)
            {
                zombosInZone.Add(zi);
                if (!attackingNow)
                {
                    attackingNow = true;
                    InvokeRepeating("Attack", 0, 2 / TowerInfo.Info.Speed); // Attack every 2 sec
                }
            }
        }
    }

    // When Zombo leaving the zone
    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag.Equals("Zombo"))
        {
            ZomboInfo zi = collider.GetComponent<ZomboInfo>();
            zombosInZone.Remove(zi);
            if (zombosInZone.Count == 0)
            {
                CancelInvoke();
                attackingNow = false;
            }
        }
    }
}
