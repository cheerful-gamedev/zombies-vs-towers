﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TowerInfo))]
public class Shooter : MonoBehaviour
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private float minDistance = 2, maxDistance = 25;
    private TowerInfo TowerInfo;
    private Transform aim;
    private RaycastHit hit;
    private bool reloading;
    private float curDelay;

    #region All ScriptableObjects
    private GameDataKeeper GameDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        GameDataKeeper = StaticKeeper.Instance.GameDataKeeper;
        PlayersDataKeeper = StaticKeeper.Instance.PlayersDataKeeper;
        #endregion

        TowerInfo = GetComponent<TowerInfo>();
    }

    void FixedUpdate()
    {
        aim = GameDataKeeper.FarthestZombo();
        if(aim != null && !reloading)
        {
            float distance = Vector3.Distance(GameDataKeeper.FarthestZomboGetFuturePosition(transform.position, TowerInfo.Info.Energy), spawnPoint.position);
            if(distance <= maxDistance && minDistance <= distance)
                Attack();
        }
        else if(curDelay <=0 )
        {
            reloading = false;
        }
        else
        {
            curDelay -= Time.fixedDeltaTime;
        }
    }

    void Attack()
    {
        Vector2 direction = aim.position - spawnPoint.position;
        direction.Normalize();
        direction *= TowerInfo.Info.Energy;

        Instantiate(bulletPrefab, spawnPoint.position, Quaternion.identity)
            .GetComponent<Bullet>()
                .Initialize(direction, TowerInfo.Info.Damage);

        curDelay = 2 / TowerInfo.Info.Speed;
        reloading = true;
    }

    public void StopAttack()
    {
        Destroy(this);
    }
}
