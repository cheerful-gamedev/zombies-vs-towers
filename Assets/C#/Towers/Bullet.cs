﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SwfClipController))]
[RequireComponent(typeof(SwfClip))]
public class Bullet : MonoBehaviour
{
    [SerializeField] private int ID;
    [SerializeField] private GameObject explosion;
    private Vector3 direction;
    private float damage;
    private Rigidbody rigidbody;
    private SwfClip clip;
    private SwfClipController clipController;
    private SceneObjectSoundsSystem SOSS;

    // [SerializeField] private bool explosive;

    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        clip = GetComponent<SwfClip>();
        clipController = GetComponent<SwfClipController>();
        Invoke("Die", 10);

        SOSS = GetComponent<SceneObjectSoundsSystem>();
    }

    public void Initialize(Vector3 direction, float damage)
    {
        this.direction = direction;
        this.damage = damage;

        Rising();
    }

    void Rising()
    {
        if(clipController.isPlaying)
            Invoke("Rising", 0.1f);
        else
        {   
            clip.sequence = "david_"+ID+"_run_0";
            clipController.loopMode = SwfClipController.LoopModes.Loop;
            clipController.Play(true);
            rigidbody.velocity = this.direction;
        }
    }

    void Die()
    {
        // Some magic
        rigidbody.velocity = Vector3.zero;
        clip.sequence = "david_"+ID+"_explosion_0";
        clipController.loopMode = SwfClipController.LoopModes.Once;
        clipController.Play(true);
        Destroy(GetComponent<Collider>());

        if(explosion != null)
            Instantiate(explosion, transform.position, Quaternion.identity)
                .GetComponent<Explosion>()
                    .Initialize(damage);

        Destroy(gameObject, (float)clip.frameCount/30f/clipController.rateScale);
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag.Equals("Zombo"))
        {
            collider.gameObject.SendMessage("TakeDamage", damage, SendMessageOptions.RequireReceiver);
            SOSS.PlayDieSound();
            Die();
        }
    }
}
