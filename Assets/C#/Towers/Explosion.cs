﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    private float damage;

    public void Initialize(float damage)
    {
        this.damage = damage;
    }

    // When Zombo entering to the Particle's collider
    void OnParticleCollision(GameObject other)
    {
        other.SendMessage("TakeDamage", damage, SendMessageOptions.RequireReceiver);
    }
}
