﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PreLoadingScene : MonoBehaviour
{
    [SerializeField] private float delay = 0.5f;
    private StaticKeeper StaticKeeper;
    private PlayersDataKeeper PlayersDataKeeper;

    void Awake()
    {
        StaticKeeper = StaticKeeper.Instance;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
    }

    void Start()
    {
        Invoke("Skip", delay);
    }

    public void Skip()
    {
        if(PlayersDataKeeper.CurrentWinsOnPlanet.Count > 0 && PlayersDataKeeper.CurrentWinsOnPlanet[0] > 0)
            SceneManager.LoadSceneAsync("Menu");
        else
            SceneManager.LoadSceneAsync("Tutorial");
    }
}
