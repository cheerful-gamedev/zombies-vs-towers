﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;

[RequireComponent(typeof(SwfClip))]
[RequireComponent(typeof(SwfClipController))]
public class Spell : MonoBehaviour
{
    private string spellName;
    private CharacteristicType characteristicType;
    private MathType mathType;
    private float
        startValue,
        spellValue, 
        periodicity,
        duration;
    private SwfClip clip;
    private SwfClipController clipController;
    private ZomboInfo ZomboInfo;
    private float characteristicValue;
    private bool IsItActive;
    private System.Type type;

    void Awake()
    {
        clip = GetComponent<SwfClip>();
        clipController = GetComponent<SwfClipController>();
        ZomboInfo = transform.parent.GetComponent<ZomboInfo>();
    }

    public void Initialization(SpellObject spell)
    {
        if(IsItActive)
            return;
        IsItActive = true;

        type = System.Type.GetType("SceneObject");

        spellName = spell.SpellName;
        characteristicType = spell.CharacteristicType;
        mathType = spell.MathType;
        spellValue = spell.Value;
        periodicity = spell.Periodicity;
        duration = spell.Duration;
        
        clip.sequence = spellName+"_show_0";
        clipController.loopMode = SwfClipController.LoopModes.Once;
        clipController.Play(true);

        float delay = (float)clip.frameCount/30f/clipController.rateScale;
        Invoke("EnableAnimationLoop", delay);
        Invoke("Activate", 0);
        Invoke("Stop", duration);
    }

    void EnableAnimationLoop()
    {
        clip.sequence = spellName+"_idle_0";
        clipController.loopMode = SwfClipController.LoopModes.Loop;
        clipController.Play(true);
    }

    void Activate()
    {
        // Getting
        characteristicValue = (float)type.GetMethod("get_"+characteristicType.ToString())
            .Invoke(ZomboInfo.Info, null);

        startValue = characteristicValue;

        if(periodicity != 0)
            InvokeRepeating("Activate"+mathType.ToString(), 0, periodicity);
        else
            Invoke("Activate"+mathType.ToString(), 0);

    }

    void SettingValue()
    {
        // Setting
        type.GetMethod("set_"+characteristicType.ToString())
            .Invoke(ZomboInfo.Info, new object[]{characteristicValue});
    }

    void ActivatePlus()
    {
        // Upgrading
        characteristicValue += spellValue;
        SettingValue();
    }

    void ActivateMinus()
    {
        // Upgrading
        characteristicValue -= spellValue;
        SettingValue();
    }

    void ActivateMultiple()
    {
        // Upgrading
        characteristicValue *= spellValue;
        SettingValue();
    }

    void ActivateDevide()
    {
        // Upgrading
        characteristicValue /= spellValue;
        SettingValue();
    }

    void Stop()
    {
        clip.sequence = spellName+"_hide_0";
        clipController.loopMode = SwfClipController.LoopModes.Once;
        clipController.Play(true);

        if(!characteristicType.Equals("CurHealth"))
        {
            characteristicValue = startValue;
            SettingValue();
        }

        IsItActive = false;
        CancelInvoke();
    }
}
