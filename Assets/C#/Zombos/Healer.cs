﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;

[RequireComponent(typeof(ZomboInfo))]
[RequireComponent(typeof(ZomboMovement))]
public class Healer : MonoBehaviour
{
    [SerializeField] private SpellObject spell;
    private ZomboInfo ZomboInfo;
    private ZomboMovement ZomboMovement;
    private SwfClip swfClip;
    private SwfClipController swfClipController;
    private List<ZomboInfo> zombosInZone = new List<ZomboInfo>(); // ZomboInfo of Zombos in the zone
    private bool attackingNow;

    void Awake()
    {
        ZomboInfo = GetComponent<ZomboInfo>();
        ZomboMovement = GetComponent<ZomboMovement>();
        swfClip = GetComponentInChildren<SwfClip>();
        swfClipController = GetComponentInChildren<SwfClipController>();
    }

    void Attack()
    {
        try{
            foreach (ZomboInfo zi in new List<ZomboInfo>(zombosInZone))
            {
                zi.Spell.Initialization(spell);
                zombosInZone.Remove(zi); // Remove from Healer list
            }
        }
        catch{}
    }

    // When Zombo entering to the zone
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag.Equals("Zombo"))
        {
            ZomboInfo zi = collider.GetComponent<ZomboInfo>();
            if (zombosInZone.IndexOf(zi) == -1)
            {
                zombosInZone.Add(zi);
                if (!attackingNow)
                {
                    attackingNow = true;
                    InvokeRepeating("Attack", 0, 2 / ZomboInfo.Info.Speed); // Attack every 2 sec
                }
            }
        }
    }

    // When Zombo leaving the zone
    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag.Equals("Zombo"))
        {
            ZomboInfo zi = collider.GetComponent<ZomboInfo>();
            zombosInZone.Remove(zi);
            if (zombosInZone.Count == 0)
            {
                CancelInvoke();
                attackingNow = false;
            }
        }
    }
}
