﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;

[RequireComponent(typeof(SwfClipController))]
public class ZomboInfo : MonoBehaviour
{
    public SceneObject Info; // Ссылка на ScriptableObject с информацией
    public float attackDistance = .7f; // Ray direction
    public Spell Spell;
    [SerializeField] private int deathAnimationID = 0;
    private SwfClipController swfClipController;
    private SceneObjectSoundsSystem SOSS;

    void Awake()
    {
        swfClipController = GetComponentInChildren<SwfClipController>();
        SOSS = GetComponent<SceneObjectSoundsSystem>();
    }

    public void Initialization(SceneObject sceneObject)
    {
        // Create new instance of SceneObject
        sceneObject.Object = gameObject;
        Info = ScriptableObject.CreateInstance<SceneObject>();
        Info.UpdateAllData(sceneObject);
    }

    public bool TakeDamage(float damage)
    {
        Info.CurHealth -= damage/Info.Armor;
        if (Info.CurHealth <= 0)
        {
            Die();
            return true;
        }
        else
            return false;
    }

    public void Die()
    {
        SOSS.PlayDieSound();

        Destroy(GetComponent<Collider>());
        gameObject.SendMessage("StopNavMeshAgent", SendMessageOptions.DontRequireReceiver);
        gameObject.SendMessage("StopAttack", SendMessageOptions.DontRequireReceiver); // Delete attack scripts
        StaticKeeper.Instance.GameDataKeeper.SpawnedZombos.Remove(Info); // Remove from GameDataKeeper list

        swfClipController.loopMode = SwfClipController.LoopModes.Once;
        swfClipController.Play("death_"+deathAnimationID);
        Destroy(gameObject, Random.Range(1.5f, 4.5f));
    }
}
