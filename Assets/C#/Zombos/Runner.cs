﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ZomboInfo))]
[RequireComponent(typeof(ZomboMovement))]
public class Runner : MonoBehaviour
{
    public void StopAttack()
    {
        Destroy(this);
    }
}
