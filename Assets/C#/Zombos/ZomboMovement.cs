﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(ZomboInfo))]
public class ZomboMovement : MonoBehaviour
{
    [SerializeField] private Vector3 towerOffset = new Vector3(0, -0.5f, 0);
    [SerializeField] private float offset = 0.5f;

    [HideInInspector] public Transform target;
    [HideInInspector] public ZomboInfo ZomboInfo;
    [HideInInspector] public NavMeshAgent agent;
    private float agentStartSpeed;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.updateUpAxis = false;
        agent.updateRotation = false;
        ZomboInfo = GetComponent<ZomboInfo>();
        agentStartSpeed = agent.speed;
        agent.speed *= ZomboInfo.Info.Speed;
    }

    void FixedUpdate()
    {
        try
        {
            if (target.position.x < transform.position.x)
                transform.localScale = new Vector3(-1, 1, 1); // Left
            else if (target.position.x > transform.position.x)
                transform.localScale = new Vector3(1, 1, 1); // Right

            agent.speed = agentStartSpeed * ZomboInfo.Info.Speed;
        }
        catch { }
    }

    // Startng Zombo
    public void StartNavMeshAgent()
    {
        agent.isStopped = false;
        agent.SetDestination(target.position);
    }

    // Stoping Zombo
    public void StopNavMeshAgent()
    {
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
    }

    public void SetNewTarget(Transform newTarget) 
    {
        target = newTarget;
        Transform navMeshPoint = null;
        switch(ZomboInfo.Info.TargetType)
        {
            case TargetType.Tower:
                navMeshPoint = newTarget.GetChild(0); // Get NavMesh Point
                navMeshPoint.RotateAround(newTarget.position, Vector3.forward, Random.Range(0f, 360f) * Time.deltaTime);
                break;

            case TargetType.End:
                navMeshPoint = newTarget;
                break;
        }
        agent.SetDestination(navMeshPoint.position);
    }
}
