﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;

[RequireComponent(typeof(ZomboInfo))]
[RequireComponent(typeof(ZomboMovement))]
[RequireComponent(typeof(SwfClipController))]
public class Bomber : MonoBehaviour
{
    private ZomboInfo ZomboInfo;
    private ZomboMovement ZomboMovement;
    private SwfClip swfClip;
    private SwfClipController swfClipController;
    private string targetLayerName;
    private RaycastHit hit;

    void Awake()
    {
        ZomboInfo = GetComponent<ZomboInfo>();
        ZomboMovement = GetComponent<ZomboMovement>();
        swfClip = GetComponentInChildren<SwfClip>();
        swfClipController = GetComponentInChildren<SwfClipController>();
        targetLayerName = ZomboInfo.Info.TargetType.ToString();
    }

    void FixedUpdate()
    {
        Vector2 vector = ZomboMovement.target.position - transform.position;
        Physics.Raycast(transform.position, vector, out hit, ZomboInfo.attackDistance, 1 << LayerMask.NameToLayer(targetLayerName));

        if (hit.collider != null && (swfClip.sequence.Contains("run") || swfClipController.isStopped))
            Attack(hit.collider);
        else if (swfClipController.isStopped)
        {
            SendMessage("StartNavMeshAgent");
            swfClipController.loopMode = SwfClipController.LoopModes.Loop;
            if (swfClipController.isStopped)
                swfClipController.Play("zomb_" + ZomboInfo.Info.ID + "_run_0");
        }
    }

    void Attack(Collider collider)
    {
        SendMessage("StopNavMeshAgent");

        collider.SendMessage("TakeDamage", ZomboInfo.Info.Damage);
        ZomboInfo.Die();
    }

    public void StopAttack()
    {
        this.enabled = false;
        // Destroy(this);
    }
}
