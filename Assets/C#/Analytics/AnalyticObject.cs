﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="My Objects/Analytic Object", fileName="Analytic Object")]
[System.Serializable]
public class AnalyticObject : ScriptableObject
{
    public string logPath = "";
    public int iterationSpeed = 10;
    public int neededIterationsNumber = 10, curIterationsNumber;
    public int earnCoins, earnDiamonds;
    public int curWins, curLoses;
    public PlanetObject analyticPlanet;
    [Header("Crowd settings")]
    public List<ZomboSelected> zombos = new List<ZomboSelected>();
    public int curSelectedZombo;
    public int curSpawnedCount;

    [Header("Info for tester")]
    [SerializeField] private float winPercent;
    [SerializeField] private float averageCoinsPerWin, averageDiamondsPerWin;

    public void ResetAllTimelessVariables()
    {
        Time.timeScale = 1;
        curIterationsNumber = 0;
        earnCoins = 0;
        earnDiamonds = 0;
        curWins = 0;
        curLoses = 0;
        curSelectedZombo = 0;
        curSpawnedCount = 0;
        averageCoinsPerWin = 0;
        averageDiamondsPerWin = 0;
        winPercent = 0;
    }

    public int ChooseZomboID()
    {
        int ZomboID = zombos[curSelectedZombo].ID;

        curSpawnedCount++;
        if(curSpawnedCount >= zombos[curSelectedZombo].CountInCrowd)
        {
            NextZomboID();
            curSpawnedCount = 0;
        }
        return ZomboID;
    }

    public void NextZomboID()
    {
        curSelectedZombo = curSelectedZombo+1 >= zombos.Count ? 0 : curSelectedZombo+1;
        if(zombos[curSelectedZombo].CountInCrowd == 0)
            NextZomboID();
    }

    public void Win(int coins, int diamonds)
    {
        curWins++;
        earnCoins += coins;
        earnDiamonds += diamonds;
        ReloadScene();
    }

    public void Lose()
    {
        curLoses++;
        ReloadScene();
    }

    void ReloadScene()
    {
        curIterationsNumber++;

        winPercent = (float)curWins / (float)curIterationsNumber * 100f;
        if(curWins > 0)
        {
            averageCoinsPerWin = earnCoins/curWins;
            averageDiamondsPerWin = earnDiamonds/curWins;
        }

        curSelectedZombo = 0;
        curSpawnedCount = 0;

        if(curIterationsNumber < neededIterationsNumber)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
        }
        else
        {
            Time.timeScale = 1;
            UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
        }
    }

    [System.Serializable]
    public class ZomboSelected
    {
        public int ID;
        public int CountInCrowd = 1;
    }
}
