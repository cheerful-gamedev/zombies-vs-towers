﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="My Objects/SpellObject", fileName="SpellObject")]
[System.Serializable]
public class SpellObject : ScriptableObject
{
    #region Variables for safe
    [SerializeField] private string spellName;
    [SerializeField] private CharacteristicType characteristicType;
    [SerializeField] private MathType mathType;
    [SerializeField] private float value;
    [SerializeField] private float periodicity;
    [SerializeField] private float duration;
    #endregion

    #region Calling methods for getting and setting
    public string SpellName {get { return spellName;} set{ spellName = value;}}
    public CharacteristicType CharacteristicType {get { return characteristicType;} set{ characteristicType = value;}}
    public MathType MathType {get { return mathType;} set{ mathType = value;}}
    public float Value {get { return this.value;} set{ this.value = value;}}
    public float Periodicity {get { return periodicity;} set{ periodicity = value;}}
    public float Duration {get { return duration;} set{ duration = value;}}
    #endregion
}
