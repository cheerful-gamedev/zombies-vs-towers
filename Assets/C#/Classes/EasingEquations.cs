﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasingEquations : MonoBehaviour
{

    public static float EaseOutSine(float time, float startValue, float change, float duration)
    {
	    return change * Mathf.Sin(time/duration * (Mathf.PI/2f)) + startValue;
    }

    public static float EaseInOutQuad(float time, float startValue, float change, float duration)
    {
        time /= duration/2;
        
        if (time < 1) return change/2*time*time + startValue;
        
        time--;
        return -change/2 * (time*(time-2) - 1) + startValue;
    }
}
