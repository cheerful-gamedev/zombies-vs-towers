﻿using UnityEngine;

[CreateAssetMenu(menuName= "My Objects/UpgradeObject", fileName= "UpgradeObject")]
public class UpgradeObject : ScriptableObject
{
    #region Variables for safe
    [SerializeField] private int id;
    [SerializeField] private MoneyType moneyType;
    [SerializeField] private int price;
    [SerializeField] private CharacteristicType characteristicType;
    [Tooltip("коэффициент умножения значения характеристики")]
    [SerializeField] private float increasedValue;
    [SerializeField] private UpgradeTargetType upgradeTargetType;
    [SerializeField] private int upgradeTypeID;
    #endregion

    #region Calling methods for getting and setting
    public int ID {get { return id;} set{id = value;}}
    public MoneyType MoneyType {get { return moneyType;} set{moneyType = value;}}
    public int Price {get { return price;} set{price = value;}}
    public CharacteristicType CharacteristicType {get { return characteristicType;} set{characteristicType = value;}}
    public float IncreasedValue {get { return increasedValue;} set{increasedValue = value;}}
    public UpgradeTargetType UpgradeTargetType {get { return upgradeTargetType;} set{upgradeTargetType = value;}}
    public int UpgradeTypeID {get { return upgradeTypeID;} set{upgradeTypeID = value;}}
    #endregion
}
