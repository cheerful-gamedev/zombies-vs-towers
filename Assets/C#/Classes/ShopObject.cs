﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;

[CreateAssetMenu(menuName = "My Objects/ShopObject", fileName = "ShopObject")]
[System.Serializable]
public class ShopObject : ScriptableObject
{
    #region Variables for safe
    [SerializeField] private ObjectType objectType;
    [SerializeField] private SceneObject sceneObject;
    [SerializeField] private MoneyType moneyType;
    [SerializeField] private int price;
    [SerializeField] private bool available;
    #endregion

    #region Calling methods for getting and setting
    public ObjectType ObjectType { get {return objectType; } set { objectType = value; } }
    public SceneObject SceneObject { get { return sceneObject; } set { sceneObject = value; } }
    public MoneyType MoneyType { get {return moneyType; } set { moneyType = value; } }
    public int Price { get { return price; } set { price = value; } }
    public bool Available { get { return available; } set { available = value; } }
    #endregion
}
