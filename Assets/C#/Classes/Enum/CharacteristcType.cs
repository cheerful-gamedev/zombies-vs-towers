﻿public enum CharacteristicType
{
    CurHealth,
    Damage,
    Speed,
    Armor,
    SpawnerSpeed,
    Coin,
    Diamond
}