﻿public enum RewardType
{
    DoubleCoins = 0,
    DoubleDiamonds,
    AllCurrency,
    BonusEnergy
}