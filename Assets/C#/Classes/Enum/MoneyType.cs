﻿public enum MoneyType
{
    Coin,
    Diamond,
    Video,
    RealCash
}