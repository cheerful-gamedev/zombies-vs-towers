﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName="My Objects/PurchaseObject", fileName="PurchaseObject")]
[System.Serializable]
public class PurchaseObject : ScriptableObject
{
    #region Variables for safe
    [SerializeField] private int id;
    [SerializeField] private MoneyType moneyType = MoneyType.RealCash;
    [SerializeField] private float price;
    [SerializeField] private MoneyType increaseMoneyType = MoneyType.Coin;
    [SerializeField] private int increaseValue;
    [SerializeField] private Sprite sprite;
    [SerializeField] private Sprite buttonSprite;
    #endregion

    #region Calling methods for getting and setting
    public int ID{get { return id;} set{id = value;}}
    public MoneyType MoneyType {get { return moneyType;} set{moneyType = value;}}
    public float Price{get { return price;} set{price = value;}}
    public MoneyType IncreaseMoneyType {get { return increaseMoneyType;} set{increaseMoneyType = value;}}
    public int IncreaseValue{get {return increaseValue;} set{increaseValue = value;}}
    public Sprite Sprite{get {return sprite;} set{sprite = value;}}
    public Sprite ButtonSprite{get {return buttonSprite;} set{buttonSprite = value;}}
    #endregion
}
