﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="My Objects/SceneObject", fileName="SceneObject")]
[System.Serializable]
public class SceneObject : ScriptableObject
{
    public void UpdateAllData(SceneObject sceneObject)
    {
        this.objectType = sceneObject.objectType;
        this.targetType = sceneObject.targetType;
        this.id = sceneObject.id;
        this.health = sceneObject.health;
        this.curHealth = this.health;
        this.damage = sceneObject.damage;
        this.speed = sceneObject.speed;
        this.armor = sceneObject.armor;
        this.energy = sceneObject.energy;
        this.pointForDestruction = sceneObject.pointForDestruction;
        this.obj = sceneObject.obj; // Object into scene
        this.prefab = sceneObject.prefab;
    }

    #region Variables for safe
    [SerializeField] private ObjectType objectType;
    [SerializeField] private TargetType targetType;
    [SerializeField] private int id;
    [SerializeField] private float health;
    [SerializeField] private float curHealth;
    [SerializeField] private float damage;
    [SerializeField] private float speed;
    [SerializeField] private float armor;
    [SerializeField] private int energy;
    [SerializeField] private int pointForDestruction = 1;
    [SerializeField] private GameObject obj; // Object into scene
    [SerializeField] private GameObject prefab;
    #endregion

    #region Calling methods for getting and setting
    public ObjectType ObjectType {get { return objectType;} set{objectType = value;}}
    public TargetType TargetType { get { return targetType; } set { targetType = value; } }
    public int ID{get { return id;} set{id = value;}}
    public float Health{get { return health;} set{health = value;}}
    public float CurHealth{get { return curHealth;} set{curHealth = value;}}
    public float Damage{get { return damage;} set{damage = value;}}
    public float Speed{get { return speed;} set{speed = value;}}
    public float Armor{get { return armor;} set{armor = value;}}
    public int Energy{get { return energy;} set{energy = value;}}
    public int PointForDestruction{get { return pointForDestruction;} set{pointForDestruction = value;}}
    public GameObject Object{get { return obj;} set{obj = value;}}
    public GameObject Prefab{get { return prefab;} set{prefab = value;}}
    #endregion
}
