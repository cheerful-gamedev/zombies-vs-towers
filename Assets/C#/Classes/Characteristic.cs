﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Characteristic
{
    public CharacteristicType CharacteristicType;
    public Button Button;
    public Image Image;
    public Text Price;
    public Text Label;
    public SaluteButton SaluteButton;
}
