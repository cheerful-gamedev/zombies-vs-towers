﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;


[CreateAssetMenu(menuName="My Objects/PlanetObject", fileName="PlanetObject")]
[System.Serializable]
public class PlanetObject : ScriptableObject
{
    #region Variables for safe
    [SerializeField] private int id = 1;
    [Header("Availability settings")]
    [SerializeField] private float moneyCoefficient;
    [SerializeField] private MoneyType moneyType;
    [SerializeField] private int price;
    [SerializeField] private int requiredWins;
    [SerializeField] private int currentWins;
    [SerializeField] private Sprite sprite;
    [SerializeField] private bool available;
    [SerializeField] private bool boughten;
    [Tooltip("Write IDs, like this: '0,1'")]
    [SerializeField] private string opensZombos, opensPlanets;

    [Header("For Level Generation")]
    [SerializeField] private TileBase baseHexagon; // Плитка
    [SerializeField] private Sprite background;

    [Tooltip("Write IDs, like this: '0,1'")]
    [SerializeField] private string availableTowers, availableTraps;
    [SerializeField] private float towerDamageCoefficient = 1;
    [SerializeField] private int oneLineLenght = 30; // Длина одного длока дороги
    [SerializeField] private int roadWidth = 12; // Ширина одного блока дороги
    [SerializeField] private int roadItereaion = 1; // Кол-во блоков дороги
    [SerializeField] private int maskDepth = 2; // Mask Size
    #endregion

    #region Calling methods for getting and setting
    public int ID{get { return id;} set{id = value;}}
    public float MoneyCoefficient{get { return moneyCoefficient;} set{moneyCoefficient = value;}}
    public MoneyType MoneyType{get { return moneyType;} set{moneyType = value;}}
    public int Price{get { return price;} set{price = value;}}
    public int RequiredWins{get { return requiredWins;} set{requiredWins = value;}}
    public int CurrentWins{get { return currentWins;} set{currentWins = value;}}
    public Sprite Sprite{get { return sprite;} set{sprite = value;}}
    public Sprite Background{get { return background;} set{background = value;}}
    public bool Available { get { return available;} set { available = value; } }
    public bool Boughten { get { return boughten;} set { boughten = value; } }
    public string OpensZombos { get { return opensZombos;} set { opensZombos = value; } }
    public string OpensPlanets { get { return opensPlanets;} set { opensPlanets = value; } }
    public TileBase BaseHexagon { get { return baseHexagon;} set { baseHexagon = value; } }
    public string AvailableTowers { get { return availableTowers;} set { availableTowers = value; } }
    public string AvailableTraps { get { return availableTraps;} set { availableTraps = value; } }
    public float TowerDamageCoefficient { get { return towerDamageCoefficient;} set { towerDamageCoefficient = value; } }
    public int OneLineLenght { get { return oneLineLenght;} set { oneLineLenght = value; } }
    public int RoadWidth { get { return roadWidth;} set { roadWidth = value; } }
    public int RoadItereaion { get { return roadItereaion;} set { roadItereaion = value; } }
    public int MaskDepth { get { return maskDepth;} set { maskDepth = value; } }
    #endregion
}
