﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;

[RequireComponent(typeof(SwfClipController))]
public class SaluteButton : MonoBehaviour
{
    private SwfClip clip;
    private SwfClipController clipController;

    void Awake()
    {
        clip = GetComponent<SwfClip>();
        clipController = GetComponent<SwfClipController>();
    }

    public void PlaySalute()
    {
        clipController.Play(true);
    }

    public void PlayCoin()
    {
        clip.sequence = "Coin";
        clipController.Play(true);
    }

    public void PlayDiamond()
    {
        clip.sequence = "Diamond";
        clipController.Play(true);
    }

    public void ChangeToCoin()
    {
        clip.sequence = "Coin";
    }

    public void ChangeToDiamond()
    {
        clip.sequence = "Diamond";
    }
}
