﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneralTutorial : MonoBehaviour
{
    [SerializeField] private bool test; // Просто для теста
    [SerializeField] private TutorialManager tutorialManager;
    [SerializeField] private InputController inputController;
    private int curTutorID;
    private float lastTimeScale = 1;

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private GameDataKeeper GameDataKeeper;
    private DataKeeper DataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        GameDataKeeper = StaticKeeper.GameDataKeeper;
        DataKeeper = StaticKeeper.DataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        #endregion

        if(!test && PlayersDataKeeper.CurrentWinsOnPlanet.Count > 0 && PlayersDataKeeper.CurrentWinsOnPlanet[0] > 0)
            SceneManager.LoadScene("Menu");
        else
        {
            lastTimeScale = Time.timeScale;
            PlayersDataKeeper.SelectedPlanetID = 0;
        }
    }

    void CheckResultsOfTutorial()
    {
        if(curTutorID == 1 && GameDataKeeper.Energy < PlayersDataKeeper.Energy)
        {
            Time.timeScale = 0;
            inputController.enabled = false;
            tutorialManager.gameObject.SetActive(true);
            tutorialManager.Show(TutorialType.General, 1);
        }
        else if(curTutorID == 2)
        {
            Time.timeScale = 0;
            inputController.enabled = false;
            tutorialManager.gameObject.SetActive(true);
            tutorialManager.Show(TutorialType.Tower, 0);
        }
    }

    public void NextTutorial()
    {
        Time.timeScale = lastTimeScale;
        inputController.enabled = true;
        CancelInvoke();
        InvokeRepeating("CheckResultsOfTutorial", 3, 3);
        curTutorID++;
    }
}
