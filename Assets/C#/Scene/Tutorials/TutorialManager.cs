﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTRuntime;
using FTRuntime.Yields;

public class TutorialManager : MonoBehaviour
{
    [SerializeField] private bool isItMenu;
    [SerializeField] private SwfClip clip;
    [SerializeField] private SwfClipController clipController;

    [Header("General")]
    [SerializeField] private SwfClipAsset generalTutorClip;
    private int[] generalTutorsID;
    private int generatlTutorCount = 2;
    [Header("Zombos")]
    [SerializeField] private SwfClipAsset zomboTutorClip;
    private int[] zomboTutorsID;
    private int zomboTutorCount;
    [Header("Towers")]
    [SerializeField] private SwfClipAsset towerTutorClip;
    private int[] towerTutorsID;
    private int towerTutorCount;
    private int curTutorialIndex;
    private int tutorialTypeCount, tutorialTypeIndex;
    private TutorialType curTutorialType;

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private GameDataKeeper GameDataKeeper;
    private DataKeeper DataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        GameDataKeeper = StaticKeeper.GameDataKeeper;
        DataKeeper = StaticKeeper.DataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        #endregion
    }

    public void SetIDs(int[] generalIDs, int[] zomboIDs, int[] towerIDs)
    {
        generalTutorsID = generalIDs;
        zomboTutorsID = zomboIDs;
        towerTutorsID = towerIDs;

        zomboTutorCount = zomboIDs.Length;
        towerTutorCount = towerIDs.Length;
    }

    void Start()
    {
        if(isItMenu) return;

        int curPlanet = PlayersDataKeeper.SelectedPlanetID;
        if(curPlanet == -1)
        {
            Hide();
            return;
        }

        if(PlayersDataKeeper.CurrentWinsOnPlanet[curPlanet] < 1)
            Show(TutorialType.Tower, curPlanet);
        else
        {
            Hide();
        }
    }

    void OnEnable()
    {
        if(isItMenu)
        {
            List<int> tmp = new List<int>();
            int[] generalIDs = new int[]{0,1};

            // Get Zombo IDs
            PlayersDataKeeper.BoughtenZombos.ForEach(zmb => tmp.Add(zmb.SceneObject.ID));
            int[] zomboIDs = tmp.ToArray();
            tmp.Clear();

            // Get tower IDs
            PlayersDataKeeper.BoughtenPlanets.ForEach(twr => {
                if(twr.ID != -1)
                    tmp.Add(twr.ID);
            });
            int[] towerIDs = tmp.ToArray();
            tmp.Clear();

            SetIDs(generalIDs, zomboIDs, towerIDs);
        }
        
        tutorialTypeIndex = 0;
        tutorialTypeCount = Enum.GetNames(typeof(TutorialType)).Length;
        curTutorialType = TutorialType.General;
    }

    public void Show()
    {
        gameObject.SetActive(true);

        if(isItMenu) return;
        int curPlanet = PlayersDataKeeper.SelectedPlanetID;
        Show(TutorialType.Tower, curPlanet);    
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void NextTutorial()
    {
        int id = curTutorialIndex + 1;
        switch(curTutorialType)
        {
            case TutorialType.General:
                if(id >= generatlTutorCount) // Если анимации этой категории закончились
                {
                    tutorialTypeIndex++; // Меняем категорию
                    id = 0; // Возвращаемся к первой анимации
                    curTutorialIndex = id;
                }
                else
                {
                    curTutorialIndex = generalTutorsID[id];
                }
            break;

            case TutorialType.Zombo:
                if(id >= zomboTutorCount) // Если анимации этой категории закончились
                {
                    tutorialTypeIndex++; // Меняем категорию
                    id = 0; // Возвращаемся к первой анимации
                    curTutorialIndex = id;
                }
                else
                {
                    curTutorialIndex = id;
                    id = zomboTutorsID[curTutorialIndex];
                }
            break;
            
            case TutorialType.Tower:
                if(id >= towerTutorCount) // Если анимации этой категории закончились
                {
                    tutorialTypeIndex++; // Меняем категорию
                    id = 0; // Возвращаемся к первой анимации
                    curTutorialIndex = id;
                }
                else
                {
                    curTutorialIndex = id;
                    id = towerTutorsID[curTutorialIndex];
                }
            break;
        }
        
        if(!isItMenu && tutorialTypeIndex >= tutorialTypeCount)
        {
            Hide();
            return;   
        }

        tutorialTypeIndex = tutorialTypeIndex >= tutorialTypeCount ? 0 : tutorialTypeIndex;
        curTutorialType = (TutorialType) tutorialTypeIndex;
        Show(curTutorialType, id);
    }

    // Показываем туториал определенной категории и индексом
    public void Show(TutorialType type, int id)
    {
        switch(type)
        {
            case TutorialType.General:
                clip.clip = generalTutorClip;
            break;

            case TutorialType.Zombo:
                clip.clip = zomboTutorClip;
            break;
            
            case TutorialType.Tower:
                clip.clip = towerTutorClip;
            break;
        }

        clip.sequence = clip.clip.Sequences[id].Name;
        clipController.Play(true);
    }
}
