﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PurchasePanel : MonoBehaviour
{
    public Button buyButton;
    public Image mainImage;
    public Image buttonImage;
    public Text price;
    public Text count; // Increase value
    public SaluteButton SaluteButton;
}
