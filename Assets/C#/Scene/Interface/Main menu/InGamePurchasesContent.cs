﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGamePurchasesContent : MonoBehaviour
{
    [SerializeField] private GameObject purchasePanelPrefab;
    [SerializeField] private GameObject content;
    private List<GameObject> displayedPurchases = new List<GameObject>();

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private DataKeeper DataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        DataKeeper = StaticKeeper.DataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        #endregion
    }

    public void Show()
    {
        RefreshMoney();
        
        foreach (PurchaseObject purchase in ShopDataKeeper.Purchases)
            AddPurchasePanel(purchase);
    }

    public void Hide()
    {
        foreach (GameObject zombo in displayedPurchases)
            Destroy(zombo);

        displayedPurchases.Clear();
    }

    void AddPurchasePanel(PurchaseObject purchase)
    {
        int curZomboID = purchase.ID;

        GameObject newPurchasePanel = Instantiate(purchasePanelPrefab, content.transform);
        displayedPurchases.Add(newPurchasePanel);

        PurchasePanel purchasePanel = newPurchasePanel.GetComponent<PurchasePanel>();
        purchasePanel.mainImage.sprite = purchase.Sprite;
        purchasePanel.buttonImage.sprite = purchase.ButtonSprite;
        purchasePanel.price.text = "$" + purchase.Price;
        purchasePanel.count.text = purchase.IncreaseValue.ToString();
        purchasePanel.buyButton.onClick.AddListener(delegate{this.BuyPurchase(purchase);});
    }

    public void BuyPurchase(PurchaseObject purchase)
    {
        switch(purchase.IncreaseMoneyType)
        {
            case MoneyType.Coin:
                PlayersDataKeeper.Coin += purchase.IncreaseValue;
            break;

            case MoneyType.Diamond:
                PlayersDataKeeper.Diamond += purchase.IncreaseValue;
            break;
        }

        StaticKeeper.UpdateAllData();
        RefreshMoney();
    }

    void RefreshMoney()
    {
        transform.parent.parent.SendMessage("RefreshMoney");
    }
}
