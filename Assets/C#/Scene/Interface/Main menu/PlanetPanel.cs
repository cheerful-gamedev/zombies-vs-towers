﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetPanel : MonoBehaviour
{
    public Image image;
    public Button button, buyButton;
    public Text progress, price;
    public SaluteButton SaluteButton;
}
