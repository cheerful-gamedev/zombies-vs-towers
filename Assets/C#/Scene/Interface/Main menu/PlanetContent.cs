﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlanetContent : MonoBehaviour
{
    [SerializeField] private GameObject planetPanelPrefab;
    [SerializeField] private GameObject content;
    [SerializeField] private Sprite[] moneyTypeSprites = new Sprite[2];
    private List<GameObject> displayedPlanets = new List<GameObject>();

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private DataKeeper DataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    private AnalyticObject AnalyticObject;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        DataKeeper = StaticKeeper.DataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        AnalyticObject = StaticKeeper.AnalyticObject;
        #endregion

        StaticKeeper.CheckBoughtenPlanets();
    }

    public void Show()
    {
        RefreshMoney();

        foreach(PlanetObject planet in PlayersDataKeeper.BoughtenPlanets)
        {
            PlanetObject plt = ShopDataKeeper.Planets.Find(plnt => plnt.ID == planet.ID);
            plt.Boughten = true;
            plt.Available = true;
        }

        foreach (PlanetObject planet in ShopDataKeeper.Planets)
            AddPlanetPanel(planet);
    }

    public void Hide()
    {
        foreach (GameObject planet in displayedPlanets)
            Destroy(planet);

        displayedPlanets.Clear();

        StaticKeeper.Instance.UpdateAllData();
    }

    void AddPlanetPanel(PlanetObject planet)
    {
        GameObject newPlanetPanel = Instantiate(planetPanelPrefab, content.transform);
        displayedPlanets.Add(newPlanetPanel);

        PlanetPanel planetPanel = newPlanetPanel.GetComponent<PlanetPanel>();

        if (planet.Available)
        {
            if(planet.Boughten)
            {
                // Load Game scene
                planetPanel.button.onClick.AddListener(
                    delegate {
                        if(planet.ID == -1)
                            AnalyticObject.ResetAllTimelessVariables();
                        PlayersDataKeeper.SelectedPlanetID = planet.ID;
                        StaticKeeper.SoundSystem.PlaySound("tap");
                        SceneManager.LoadScene("Game");
                    }
                );
            }
            else
            {
                planetPanel.progress.transform.parent.gameObject.SetActive(false);

                // Buy Planet
                planetPanel.buyButton.onClick.AddListener( delegate { BuyPlanet(planet.ID); } );
                planetPanel.buyButton.gameObject.SetActive(true);

                // Change buyButton background
                switch(planet.MoneyType)
                {
                    case MoneyType.Coin:
                        planetPanel.buyButton.image.sprite = moneyTypeSprites[0];
                        break;

                    case MoneyType.Diamond:
                        planetPanel.buyButton.image.sprite = moneyTypeSprites[1];
                        break;
                }

                planetPanel.price.text = planet.Price.ToString();
            }

            planetPanel.image.sprite = planet.Sprite;
            
            // If all needed wins was completed
            if(planet.CurrentWins >= planet.RequiredWins)
            {
                Destroy(planetPanel.progress.transform.parent.gameObject);
                // planetPanel.progress.text = "done";
            }
            else // If not
            {
                planetPanel.progress.text = planet.CurrentWins+"/"+planet.RequiredWins;
            }
        }
        else
        {
            planetPanel.button.interactable = false;
            planetPanel.progress.transform.parent.gameObject.SetActive(false);
        }
    }

    public void BuyPlanet(int planetID)
    {
        // Have not this Planet
        PlanetObject planetFromShopDataKeeper = ShopDataKeeper.Planets.Find(planet => planet.ID == planetID);
        if(PlayersDataKeeper.BoughtenPlanets.Find(planet => planet.ID == planetID) == null)
        {
            switch (planetFromShopDataKeeper.MoneyType)
            {
                case MoneyType.Coin:
                    if(PlayersDataKeeper.Coin >= planetFromShopDataKeeper.Price)
                    {
                        PlayersDataKeeper.Coin -= planetFromShopDataKeeper.Price;
                        PlayersDataKeeper.BoughtenPlanets.Add(planetFromShopDataKeeper);
                        Hide();
                        Show();
                        StaticKeeper.SoundSystem.PlaySound("buy");
                    }
                    else
                    {
                        Debug.Log("Не хватает монет");
                        StaticKeeper.SoundSystem.PlaySound("buy error");
                    }
                    break;

                case MoneyType.Diamond:
                    if (PlayersDataKeeper.Diamond >= planetFromShopDataKeeper.Price)
                    {
                        PlayersDataKeeper.Diamond -= planetFromShopDataKeeper.Price;
                        PlayersDataKeeper.BoughtenPlanets.Add(planetFromShopDataKeeper);
                        Hide();
                        Show();
                        StaticKeeper.SoundSystem.PlaySound("buy");
                    }
                    else
                    {
                        Debug.Log("Не хватает алмазов");
                        StaticKeeper.SoundSystem.PlaySound("buy error");
                    }
                    break;
            }
        }
    }

    void RefreshMoney()
    {
        transform.parent.parent.SendMessage("RefreshMoney");
    }
}
