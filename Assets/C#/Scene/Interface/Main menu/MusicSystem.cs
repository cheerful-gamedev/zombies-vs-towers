﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicSystem : MonoBehaviour
{
    private AudioSource source;
    private StaticKeeper StaticKeeper;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        StaticKeeper = StaticKeeper.Instance;
    }

    void Start() { CheckMute(); }

    public void CheckMute() { source.mute = !StaticKeeper.PlayersDataKeeper.MusicOn; }
}
