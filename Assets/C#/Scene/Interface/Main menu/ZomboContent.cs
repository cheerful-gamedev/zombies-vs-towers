﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZomboContent : MonoBehaviour
{
    [SerializeField] private GameObject zombosPanelPrefab;
    [SerializeField] private GameObject content;
    [SerializeField] private ZombosUpgradesContent ZombosUpgradesContent;
    [SerializeField] private Sprite[] moneyTypeSprites = new Sprite[2];

    private List<GameObject> displayedZombos = new List<GameObject>();
    private List<int> displayedZombos_ID = new List<int>();

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private DataKeeper DataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        DataKeeper = StaticKeeper.Instance.DataKeeper;
        ShopDataKeeper = StaticKeeper.Instance.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.Instance.PlayersDataKeeper;
        #endregion

        StaticKeeper.CheckBoughtenZombos();
    }

    public void Show()
    {
        RefreshMoney();
        
        foreach (ShopObject zombo in ShopDataKeeper.Zombos.FindAll(z => z.Available))
            AddZomboPanel(zombo);

        ShopObject unavailableZombo = ShopDataKeeper.Zombos.Find(z => !z.Available);
        if(unavailableZombo != null)
        {
            AddZomboPanel(unavailableZombo);
            AddZomboPanel(unavailableZombo);
        }
    }

    public void Hide()
    {
        foreach (GameObject zombo in displayedZombos)
            Destroy(zombo);

        displayedZombos.Clear();
        displayedZombos_ID.Clear();

        StaticKeeper.Instance.UpdateAllData();
    }

    void AddZomboPanel(ShopObject zombo)
    {
        int curZomboID = zombo.SceneObject.ID;
        if (displayedZombos_ID.IndexOf(curZomboID) != -1 && PlayersDataKeeper.BoughtenZombos.Find(z => z.SceneObject.ID == curZomboID) != null)
            return;

        GameObject newZombosPanel = Instantiate(zombosPanelPrefab, content.transform);
        displayedZombos.Add(newZombosPanel);
        displayedZombos_ID.Add(curZomboID);

        ZomboPanel zomboPanel = newZombosPanel.GetComponent<ZomboPanel>();
        zomboPanel.clip.sequence = "zomb_" + curZomboID + "_idle_0";

        // Locked Zombo
        if (!zombo.Available)
        {
            zomboPanel.clip.sequence = "zomb_locked_0";
        }
        // All free Zombo
        else if(PlayersDataKeeper.BoughtenZombos.Find(obj => obj.SceneObject == zombo.SceneObject) != null)
        {
            zomboPanel.locked.SetActive(false);

            Button upgradeButton = zomboPanel.upgradeButton;
            upgradeButton.gameObject.SetActive(true);
            upgradeButton.onClick.AddListener(delegate{ this.ZombosUpgradesContentShow(curZomboID); });
        }
        // Every Zombo didn't buy
        else if(PlayersDataKeeper.BoughtenZombos.Find(obj => obj.SceneObject == zombo.SceneObject) == null)
        {
            zomboPanel.locked.SetActive(false);

            Button buyButton = zomboPanel.buyButton;
            buyButton.gameObject.SetActive(true);
            buyButton.onClick.AddListener(delegate{ this.BuyZombo(curZomboID); });

            // Change buyButton background
            switch(zombo.MoneyType)
            {
                case MoneyType.Coin:
                    buyButton.image.sprite = moneyTypeSprites[0];
                    break;

                case MoneyType.Diamond:
                    buyButton.image.sprite = moneyTypeSprites[1];
                    break;
            }

            zomboPanel.price.text = zombo.Price.ToString();
        }

        /* Куплен -> отображать Upgrade button
         * добавить методы HideAll и ZombosUpgradesContentShow из MainMenu.cs в Button */
        /* Не куплен -> отображать Buy button
         * добавить метод покупки Зомбо в Button */
        /* Закрыт -> отображать Locker */
    }

    public void ZombosUpgradesContentShow(int zomboID)
    {
        // transform.parent.parent.SendMessage("HideAll");
        ZombosUpgradesContent.Show(zomboID);
    }

    public void BuyZombo(int zomboID)
    {
        // Have not this Zombo
        ShopObject zomboFromShopDataKeeper = ShopDataKeeper.Zombos.Find(zombo => zombo.SceneObject.ID == zomboID);
        if(PlayersDataKeeper.BoughtenZombos.Find(zmb => zmb.SceneObject.ID == zomboID) == null)
        {
            switch (zomboFromShopDataKeeper.MoneyType)
            {
                case MoneyType.Coin:
                    if(PlayersDataKeeper.Coin >= zomboFromShopDataKeeper.Price)
                    {
                        PlayersDataKeeper.Coin -= zomboFromShopDataKeeper.Price;
                        PlayersDataKeeper.BoughtenZombos.Add(zomboFromShopDataKeeper);
                        Hide();
                        Show();
                        StaticKeeper.SoundSystem.PlaySound("buy");
                    }
                    else
                    {
                        Debug.Log("Не хватает монет");
                        StaticKeeper.SoundSystem.PlaySound("buy error");
                    }
                    break;

                case MoneyType.Diamond:
                    if (PlayersDataKeeper.Diamond >= zomboFromShopDataKeeper.Price)
                    {
                        PlayersDataKeeper.Diamond -= zomboFromShopDataKeeper.Price;
                        PlayersDataKeeper.BoughtenZombos.Add(zomboFromShopDataKeeper);
                        Hide();
                        Show();
                        StaticKeeper.SoundSystem.PlaySound("buy");
                    }
                    else
                    {
                        Debug.Log("Не хватает алмазов");
                        StaticKeeper.SoundSystem.PlaySound("buy error");
                    }
                    break;
            }
        }
    }

    void RefreshMoney()
    {
        transform.parent.parent.SendMessage("RefreshMoney");
    }
}
