﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Text coins, diamonds;
    [SerializeField] private MoneyCalculation _coins, _diamonds;
    private PlanetContent PlanetContent;
    private ZomboContent ZomboContent;
    private InGamePurchasesContent InGamePurchasesContent;
    private UpgradesContent UpgradesContent;
    private ZombosUpgradesContent ZombosUpgradesContent;
    private OptionsContent OptionsContent;
    private TutorialManager TutorialManager;

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private DataKeeper DataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        DataKeeper = StaticKeeper.DataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        #endregion

        PlanetContent = GetComponentInChildren<PlanetContent>();
        ZomboContent = GetComponentInChildren<ZomboContent>();
        InGamePurchasesContent = GetComponentInChildren<InGamePurchasesContent>();
        UpgradesContent = GetComponentInChildren<UpgradesContent>();
        ZombosUpgradesContent = GetComponentInChildren<ZombosUpgradesContent>();
        OptionsContent = GetComponentInChildren<OptionsContent>();
        TutorialManager = GetComponentInChildren<TutorialManager>();

        RefreshMoney();
        StaticKeeper.CheckBoughtenZombos();
        StaticKeeper.CheckBoughtenPlanets();
    }

    void Start()
    {
        HideAll();
        ZomboContent.Show();
    }

    public void HideAll()
    {
        PlanetContent.Hide();
        ZomboContent.Hide();
        InGamePurchasesContent.Hide();
        UpgradesContent.Hide();
        ZombosUpgradesContent.Hide();
        OptionsContent.Hide();
        TutorialManager.Hide();
    }

    public void PlanetContentShow()
    {
        PlanetContent.Show();
    }

    public void ZomboContentShow()
    {
        ZomboContent.Show();
    }
    
    public void PurchaseContentShow()
    {
        InGamePurchasesContent.Show();
    }

    public void UpgradesContentShow()
    {
        UpgradesContent.Show();
    }

    // public void ZombosUpgradesContentShow()
    // {
    //     ZombosUpgradesContent.Show();
    // }

    public void OptionsContentShow()
    {
        OptionsContent.Show();
    }

    public void TutorialContentShow()
    {
        TutorialManager.Show();
    }

    public void RefreshMoney()
    {
        try {
            _coins.AmountCalculation(int.Parse(coins.text), PlayersDataKeeper.Coin);
            _diamonds.AmountCalculation(int.Parse(diamonds.text), PlayersDataKeeper.Diamond);
        }
        catch {
            _coins.AmountCalculation(0, PlayersDataKeeper.Coin);
            _diamonds.AmountCalculation(0, PlayersDataKeeper.Diamond);
        }
        // coins.text = PlayersDataKeeper.Coin.ToString();
        // diamonds.text = PlayersDataKeeper.Diamond.ToString();
    }
}
