﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradesContent : MonoBehaviour
{
    [SerializeField] private Sprite[] moneyTypeSprites = new Sprite[2];
    [SerializeField] private List<Characteristic> characteristics = new List<Characteristic>();

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private DataKeeper DataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        DataKeeper = StaticKeeper.DataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        #endregion
    }

    public void Show()
    {
        RefreshMoney();
        
        gameObject.SetActive(true);

        foreach(Characteristic characteristic in characteristics)
        {
            UpgradeObject upgrade = PlayersDataKeeper.BoughtenPlayerUpgrades.Find(up => up.CharacteristicType == characteristic.CharacteristicType);
            int upgradeID = 0;
            if (upgrade != null)
                upgradeID = upgrade.UpgradeTypeID+1;

            UpgradeObject upgradeFromShopDataKeeper = ShopDataKeeper.PlayerUpgrades.Find(up => up.CharacteristicType == characteristic.CharacteristicType && up.UpgradeTypeID == upgradeID);
            if (upgradeFromShopDataKeeper != null)
            {
                // Change buyButton background
                switch (upgradeFromShopDataKeeper.MoneyType)
                {
                    case MoneyType.Coin:
                        characteristic.Image.sprite = moneyTypeSprites[0];
                        break;

                    case MoneyType.Diamond:
                        characteristic.Image.sprite = moneyTypeSprites[1];
                        break;
                }

                characteristic.Price.text = upgradeFromShopDataKeeper.Price.ToString();
                characteristic.Label.text = "x" + upgradeFromShopDataKeeper.IncreasedValue.ToString();

            }
            else
            {
                characteristic.Image.sprite = moneyTypeSprites[2];
                characteristic.Price.text = string.Empty;
                UpgradeObject lastUpgrade = ShopDataKeeper.PlayerUpgrades.Find(up => up.CharacteristicType == characteristic.CharacteristicType && up.UpgradeTypeID == upgradeID - 1);
                if(lastUpgrade != null)
                    characteristic.Label.text = "x" + lastUpgrade.IncreasedValue;
                else
                    characteristic.Label.text = "-";
            }
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        StaticKeeper.Instance.UpdateAllData();
    }

    public void BuyUpgrade(string upgradeName)
    {
        Characteristic characteristic = characteristics.Find(ch => ch.CharacteristicType.ToString().Equals(upgradeName));
        if (characteristic == null)
            return;

        UpgradeObject upgrade = PlayersDataKeeper.BoughtenPlayerUpgrades.Find(up => up.CharacteristicType == characteristic.CharacteristicType);
        int upgradeID = 0;
        if (upgrade != null)
            upgradeID = upgrade.UpgradeTypeID + 1;

        UpgradeObject upgradeFromShopDataKeeper = ShopDataKeeper.PlayerUpgrades.Find(up => up.CharacteristicType == characteristic.CharacteristicType && up.UpgradeTypeID == upgradeID);
        if (upgradeFromShopDataKeeper != null)
        {
            System.Type type = System.Type.GetType("PlayersDataKeeper");
            switch (upgradeFromShopDataKeeper.MoneyType)
            {
                case MoneyType.Coin:
                    if(PlayersDataKeeper.Coin >= upgradeFromShopDataKeeper.Price)
                    {
                        PlayersDataKeeper.Coin -= upgradeFromShopDataKeeper.Price;
                        PlayersDataKeeper.BoughtenPlayerUpgrades.Remove(upgrade);
                        PlayersDataKeeper.BoughtenPlayerUpgrades.Add(upgradeFromShopDataKeeper);
                        // Вносим изменения в купленный параметр
                        type.GetMethod("set_K"+upgradeFromShopDataKeeper.CharacteristicType.ToString())
                            .Invoke(PlayersDataKeeper, new object[]{upgradeFromShopDataKeeper.IncreasedValue});
                        Hide();
                        Show();
                        StaticKeeper.SoundSystem.PlaySound("buy");
                    }
                    else
                    {
                        Debug.Log("Не хватает монет");
                    }
                    break;

                case MoneyType.Diamond:
                    if (PlayersDataKeeper.Diamond >= upgradeFromShopDataKeeper.Price)
                    {
                        PlayersDataKeeper.Diamond -= upgradeFromShopDataKeeper.Price;
                        PlayersDataKeeper.BoughtenPlayerUpgrades.Remove(upgrade);
                        PlayersDataKeeper.BoughtenPlayerUpgrades.Add(upgradeFromShopDataKeeper);
                        // Вносим изменения в купленный параметр
                        type.GetMethod("set_K"+upgradeFromShopDataKeeper.CharacteristicType.ToString())
                            .Invoke(PlayersDataKeeper, new object[]{upgradeFromShopDataKeeper.IncreasedValue});
                        Hide();
                        Show();
                        StaticKeeper.SoundSystem.PlaySound("buy");
                    }
                    else
                    {
                        Debug.Log("Не хватает алмазов");
                    }
                    break;
            }
        }
    }

    void RefreshMoney()
    {
        transform.parent.parent.SendMessage("RefreshMoney");
    }
}
