﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombosUpgradesContent : MonoBehaviour
{
    [SerializeField] private Sprite[] moneyTypeSprites = new Sprite[2];
    [SerializeField] private List<Characteristic> characteristics = new List<Characteristic>();

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private DataKeeper DataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        DataKeeper = StaticKeeper.Instance.DataKeeper;
        ShopDataKeeper = StaticKeeper.Instance.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.Instance.PlayersDataKeeper;
        #endregion
    }

    private int zomboID;
    public void Show(int zomboID)
    {
        RefreshMoney();
        
        gameObject.SetActive(true);
        this.zomboID = zomboID;

        foreach (Characteristic characteristic in characteristics)
        {
            UpgradeObject upgrade = PlayersDataKeeper.BoughtenUpgrades.Find(up => up.ID == zomboID && up.CharacteristicType == characteristic.CharacteristicType);
            int upgradeID = 0;
            if (upgrade != null)
                upgradeID = upgrade.UpgradeTypeID + 1;

            UpgradeObject upgradeFromShopDataKeeper = ShopDataKeeper.Upgrades.Find(up => up.ID == zomboID && up.CharacteristicType == characteristic.CharacteristicType && up.UpgradeTypeID == upgradeID);
            if (upgradeFromShopDataKeeper != null)
            {
                // Change buyButton background
                switch (upgradeFromShopDataKeeper.MoneyType)
                {
                    case MoneyType.Coin:
                        characteristic.Image.sprite = moneyTypeSprites[0];
                        break;

                    case MoneyType.Diamond:
                        characteristic.Image.sprite = moneyTypeSprites[1];
                        break;
                }

                characteristic.Price.text = upgradeFromShopDataKeeper.Price.ToString();
                characteristic.Label.text = (upgradeFromShopDataKeeper.IncreasedValue - 1f).ToString("+0.0%");

            }
            else
            {
                characteristic.Image.sprite = moneyTypeSprites[2];
                characteristic.Price.text = string.Empty;
                UpgradeObject lastUpgrade = ShopDataKeeper.Upgrades.Find(up => up.ID == zomboID && up.CharacteristicType == characteristic.CharacteristicType && up.UpgradeTypeID == upgradeID - 1);
                if(lastUpgrade != null)
                    characteristic.Label.text = (lastUpgrade.IncreasedValue).ToString("=0.0%");
                else
                    characteristic.Label.text = "( -_-)";
            }
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        StaticKeeper.Instance.UpdateAllData();
    }

    public void BuyUpgrade(string upgradeName)
    {
        Characteristic characteristic = characteristics.Find(ch => ch.CharacteristicType.ToString().Equals(upgradeName));
        if (characteristic == null)
            return;

        UpgradeObject upgrade = PlayersDataKeeper.BoughtenUpgrades.Find(up => up.ID == zomboID && up.CharacteristicType == characteristic.CharacteristicType);
        int upgradeID = 0;
        if (upgrade != null)
            upgradeID = upgrade.UpgradeTypeID + 1;

        UpgradeObject upgradeFromShopDataKeeper = ShopDataKeeper.Upgrades.Find(up => up.ID == zomboID && up.CharacteristicType == characteristic.CharacteristicType && up.UpgradeTypeID == upgradeID);
        if (upgradeFromShopDataKeeper != null)
        {
            switch (upgradeFromShopDataKeeper.MoneyType)
            {
                case MoneyType.Coin:
                    if(PlayersDataKeeper.Coin >= upgradeFromShopDataKeeper.Price)
                    {
                        PlayersDataKeeper.Coin -= upgradeFromShopDataKeeper.Price;
                        PlayersDataKeeper.BoughtenUpgrades.Remove(upgrade);
                        PlayersDataKeeper.BoughtenUpgrades.Add(upgradeFromShopDataKeeper);
                        Hide();
                        Show(zomboID);
                        StaticKeeper.SoundSystem.PlaySound("buy");
                    }
                    else
                    {
                        Debug.Log("Не хватает монет");
                        StaticKeeper.SoundSystem.PlaySound("buy error");
                    }
                    break;

                case MoneyType.Diamond:
                    if (PlayersDataKeeper.Diamond >= upgradeFromShopDataKeeper.Price)
                    {
                        PlayersDataKeeper.Diamond -= upgradeFromShopDataKeeper.Price;
                        PlayersDataKeeper.BoughtenUpgrades.Remove(upgrade);
                        PlayersDataKeeper.BoughtenUpgrades.Add(upgradeFromShopDataKeeper);
                        Hide();
                        Show(zomboID);
                        StaticKeeper.SoundSystem.PlaySound("buy");
                    }
                    else
                    {
                        Debug.Log("Не хватает алмазов");
                        StaticKeeper.SoundSystem.PlaySound("buy error");
                    }
                    break;
            }
        }
    }

    void RefreshMoney()
    {
        transform.parent.parent.SendMessage("RefreshMoney");
    }
}
