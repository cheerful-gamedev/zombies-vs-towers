﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FTRuntime;

public class ZomboPanel : MonoBehaviour
{
    public Button buyButton, upgradeButton;
    public Text price;
    public GameObject locked;
    public  SwfClip clip;
    public Image platform;
    public SaluteButton SaluteButton;
}
