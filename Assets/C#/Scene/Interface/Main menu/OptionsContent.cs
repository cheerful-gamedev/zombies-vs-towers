﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsContent : MonoBehaviour
{
    [Header("Effects")]
    [SerializeField] private Sprite[] effectsSprite = new Sprite[2];
    [SerializeField] private Image effectsImage;

    [Header("Music")]
    [SerializeField] private MusicSystem musicSystem;
    [SerializeField] private Sprite[] musicSprite = new Sprite[2];
    [SerializeField] private Image musicImage;

    [Header("Language")]
    [SerializeField] private Sprite[] languageSprite = new Sprite[2];
    [SerializeField] private Image languageImage;
    private PlayersDataKeeper PDK;

    void Awake()
    {
        PDK = StaticKeeper.Instance.PlayersDataKeeper;
        Effects(false);
        Music(false);
        if(languageImage != null)
            Language(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Effects(bool change = true)
    {
        if(change) // Switching or not
            PDK.EffectsOn = !PDK.EffectsOn;

        int effectsSpriteNumber = PDK.EffectsOn ? 1 : 0;
        effectsImage.sprite = effectsSprite[effectsSpriteNumber];
    }

    public void Music(bool change = true)
    {
        if(change) // Switching or not
        {
            PDK.MusicOn = !PDK.MusicOn;
            musicSystem.CheckMute();
        }

        int musicSpriteNumber = PDK.MusicOn ? 1 : 0;
        musicImage.sprite = musicSprite[musicSpriteNumber];
    }

    public void Language(bool change = true)
    {
        int languageSpriteNumber = PDK.FlagIndex;
        if(change) // Switching or not
            languageSpriteNumber++;
        languageSpriteNumber = languageSpriteNumber >= languageSprite.Length ? 0 : languageSpriteNumber;
        languageImage.sprite = languageSprite[languageSpriteNumber];
        PDK.FlagIndex = languageSpriteNumber;
    }

    public void Estimate()
    {
        Debug.Log("Estimate");
    }
}
