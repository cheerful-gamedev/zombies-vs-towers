﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundSystem : Singleton<SoundSystem> 
{
    [SerializeField] private List<AudioClip> sounds = new List<AudioClip>();
    private AudioSource source;
    private PlayersDataKeeper PDK;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        PDK = StaticKeeper.Instance.PlayersDataKeeper;
    }

    public void PlaySound(string soundName) 
    {
        if(!PDK.EffectsOn) return; // Если эффекты выключены

        AudioClip sound = sounds.Find(s => s.name.Equals(soundName));
        source.clip = sound;
        source.Play();
    }
}
