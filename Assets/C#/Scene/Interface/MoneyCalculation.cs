﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyCalculation : MonoBehaviour
{
    [SerializeField] private Text money;
    [SerializeField] private TextMesh moneyMesh;

    private float timeFromStart;
    private float duration = .3f, delay = 0.01f;
    private int curAmount, startAmount, endAmount, change;
    private bool active;

    public void AmountCalculation(int startAmount, int endAmount)
    {
        timeFromStart = 0;
        curAmount = startAmount;
        this.startAmount = startAmount;
        this.endAmount = endAmount;
        change = endAmount - startAmount;
        
        active = true;
    }

    void FixedUpdate()
    {
        if(!active) return;

        if(timeFromStart < duration)
        {
            curAmount = (int)EasingEquations.EaseInOutQuad(timeFromStart, startAmount, change, duration);
            if(money != null)
                money.text = curAmount.ToString();
            else
                moneyMesh.text = curAmount.ToString();
            timeFromStart += Time.fixedDeltaTime;
        }
        else
        {
            if(money != null)
                money.text = endAmount.ToString();
            else
                moneyMesh.text = endAmount.ToString();
            active = false;
        }
    }
}
