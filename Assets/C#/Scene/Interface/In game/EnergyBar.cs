﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
    [SerializeField] private float end = 220;
    [SerializeField] private RectTransform fill;
    [SerializeField] Text energyCount;
    
    public void RefreshEnergyBar(float k)
    {
        energyCount.text = k.ToString("0.0%");
        k = 1 - k;
        Vector3 fillPos = fill.anchoredPosition;
        fillPos.x = end * k;
        fill.anchoredPosition = fillPos;
    }
}
