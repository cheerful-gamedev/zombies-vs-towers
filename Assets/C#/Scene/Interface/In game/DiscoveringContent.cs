﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using FTRuntime;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class DiscoveringContent : MonoBehaviour
{
    [Header("Backgrounds")]
    [SerializeField] private SwfClipController planetDiscoveringClipController;
    [SerializeField] private SwfClipController zomboDiscoveringClipController;

    [Header("Contents")]
    [SerializeField] private SpriteRenderer planetSpriteRenderer;
    [SerializeField] private SwfClip zomboClip;
    [SerializeField] private SwfClipController zomboClipController;

    [Header("Tutorials")]
    [SerializeField] private SwfClip zomboTutorClip;
    [SerializeField] private SwfClip towerTutorClip;

    private List<int> opensPlanetsID = new List<int>();
    private List<int> opensZombosID = new List<int>();
    private int curOpensPlanetIndex = -1, curOpensZomboIndex = -1;
    private float planetWaitTime, zomboWaitTime;
    private Button button;

    #region All ScriptableObjects
    private GameDataKeeper GameDataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        GameDataKeeper = StaticKeeper.Instance.GameDataKeeper;
        ShopDataKeeper = StaticKeeper.Instance.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.Instance.PlayersDataKeeper;
        #endregion

        button = GetComponent<Button>();
    }

    public void Show()
    {
        planetWaitTime = 1.27f;
        zomboWaitTime = 1.27f;

        gameObject.SetActive(true);

        PlanetObject nextPlanet = ShopDataKeeper.Planets.Find(planet => planet.ID == PlayersDataKeeper.SelectedPlanetID);
        if(nextPlanet == null)
            return;

        // Получаем ID планет, что открываются после прохождения текущей
        nextPlanet.OpensPlanets.Split(new char[]{','}, System.StringSplitOptions.RemoveEmptyEntries).ToList()
            .ForEach(str => opensPlanetsID.Add(int.Parse(str)));

        // Получаем ID Зомбей, что открываются после прохождения текущей планеты
        nextPlanet.OpensZombos.Split(new char[]{','}, System.StringSplitOptions.RemoveEmptyEntries).ToList()
            .ForEach(str => opensZombosID.Add(int.Parse(str)));

        StartCoroutine(NextDiscovering_Ienumerator());
    }

    public void NextDiscovering()
    {
        button.interactable = false;
        StartCoroutine(NextDiscovering_Ienumerator());
    }

    IEnumerator NextDiscovering_Ienumerator()
    {
        button.interactable = false;

        // Disabling planet's content
        planetDiscoveringClipController.gameObject.SetActive(false);
        planetSpriteRenderer.gameObject.SetActive(false);

        // Disabling zombo's content
        zomboDiscoveringClipController.gameObject.SetActive(false);
        zomboClip.gameObject.SetActive(false);

        curOpensPlanetIndex++;
        if(curOpensPlanetIndex < opensPlanetsID.Count) // Открывает все планеты (их может быть несколько)
        {
            yield return StartCoroutine(ShowContentAfter(true));
        }
        else // После планет открываем Зомбей
        {
            curOpensZomboIndex++;
            if(curOpensZomboIndex < opensZombosID.Count)
            {
                yield return StartCoroutine(ShowContentAfter(false));
            }
            else
            {
                // Ending this shit
                StaticKeeper.Instance.UpdateAllData();
                SceneManager.LoadScene("Menu");
            }
        }
    }

    IEnumerator ShowContentAfter(bool isItPlanet)
    {
        if(isItPlanet) // Planet
        {
            planetDiscoveringClipController.gameObject.SetActive(true);
            planetDiscoveringClipController.Play(true);

            yield return new WaitForSeconds(planetWaitTime);

            // Debug.Log("Planet "+opensPlanetsID[curOpensPlanetIndex]);
            // Находим PlanetObject для этой планеты
            PlanetObject nextPlanet = ShopDataKeeper.Planets.Find(planet => planet.ID == opensPlanetsID[curOpensPlanetIndex]);
            if(nextPlanet == null)
            {
                StartCoroutine(NextDiscovering_Ienumerator());
            }
            else
            {
                planetSpriteRenderer.sprite = nextPlanet.Sprite;
                towerTutorClip.sequence = "tower_"+nextPlanet.ID+"_tutorial"; // Tutorial clip
                nextPlanet.Available = true;
                planetSpriteRenderer.gameObject.SetActive(true);
                towerTutorClip.gameObject.SetActive(true); // Tutorial clip
                button.interactable = true;
            }
        }
        else // Zombo
        {
            zomboDiscoveringClipController.gameObject.SetActive(true);
            zomboDiscoveringClipController.Play(true);

            yield return new WaitForSeconds(zomboWaitTime);
            
            // Debug.Log("Zombo "+opensZombosID[curOpensZomboIndex]);
            // Находим все ShopObject для этого 
            int newZomboID = opensZombosID[curOpensZomboIndex];
            List<ShopObject> nextZombo = ShopDataKeeper.Zombos.FindAll(zombo => zombo.SceneObject.ID == newZomboID);
            if(nextZombo.Count == 0)
            {
                StartCoroutine(NextDiscovering_Ienumerator());
            }
            else
            {
                zomboClip.sequence = "zomb_"+newZomboID+"_idle_0";
                zomboTutorClip.sequence = "zombo_"+newZomboID+"_tutorial"; // Tutorial clip
                nextZombo.ForEach(zmb => zmb.Available = true);
                zomboClip.gameObject.SetActive(true);
                zomboTutorClip.gameObject.SetActive(true); // Tutorial clip
                button.interactable = true;
            }
        }
    }
}
