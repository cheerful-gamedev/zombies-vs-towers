﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class PauseContent : MonoBehaviour
{
    [SerializeField] private GameObject pauseContent;

    [Header("Pause")]
    [SerializeField] private Sprite[] pauseSprite = new Sprite[2];
    private Image pauseImage;
    private Camera2DFollowTDS camera2DFollow;
    private float lastTimeScale = 1;

    void Awake()
    {
        pauseImage = GameObject.Find("In fight/Pause").GetComponent<Image>();
        camera2DFollow = GameObject.Find("Main Camera").GetComponent<Camera2DFollowTDS>();
    }

    public void Pause()
    {
        pauseContent.SetActive(true);
        lastTimeScale = Time.timeScale;
        Time.timeScale = 0;

        pauseImage.sprite = pauseSprite[1];
        camera2DFollow.enabled = false;
    }

    public void Show()
    {
        pauseContent.SetActive(true);
    }

    public void Hide()
    {
        pauseContent.SetActive(false);
    }

    public void Resume()
    {
        pauseContent.SetActive(false);
        Time.timeScale = lastTimeScale;
        pauseImage.sprite = pauseSprite[0];
        camera2DFollow.enabled = true;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = lastTimeScale;
    }

    public void MainMenuLoad()
    {
        SceneManager.LoadScene("Menu");
        Time.timeScale = lastTimeScale;
    }
}
