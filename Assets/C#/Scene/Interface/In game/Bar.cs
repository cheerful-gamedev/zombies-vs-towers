﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar : MonoBehaviour
{
    [SerializeField] private float start = 0;
    [SerializeField] private float end = 220;
    [SerializeField] private RectTransform fill;

    public void RefreshBar(float k)
    {
        Vector3 fillPos = fill.anchoredPosition;
        fillPos.x = start + (Mathf.Abs(end) + Mathf.Abs(start))* k;
        fill.anchoredPosition = fillPos;
    }
}
