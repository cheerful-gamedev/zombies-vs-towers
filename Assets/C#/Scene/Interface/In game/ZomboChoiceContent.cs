﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZomboChoiceContent : MonoBehaviour
{
    
    [SerializeField] private GameObject zomboChoicePanelPrefab;
    [SerializeField] private GameObject content;
    [SerializeField] private Sprite[] platforms = new Sprite[2];

    private List<GameObject> displayedZombos = new List<GameObject>();
    private List<ZomboPanel> displayedZombos_panels = new List<ZomboPanel>();
    private List<int> displayedZombos_ID = new List<int>();

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private DataKeeper DataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        DataKeeper = StaticKeeper.DataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        #endregion

        if(PlayersDataKeeper.SelectedPlanetID != -1 
            && PlayersDataKeeper.BoughtenZombos
                .Find(zmb => zmb.SceneObject.ID == PlayersDataKeeper.SelectedZomboID) == null)
            PlayersDataKeeper.SelectedZomboID = 0;
            
        Show();
    }

    public void Show()
    {
        foreach (ShopObject zombo in PlayersDataKeeper.BoughtenZombos)
            AddZomboPanel(zombo);

        ZomboChoice(PlayersDataKeeper.SelectedZomboID);
    }

    public void Hide()
    {
        foreach (GameObject zombo in displayedZombos)
            Destroy(zombo);

        displayedZombos.Clear();
        displayedZombos_ID.Clear();
    }

    void AddZomboPanel(ShopObject zombo)
    {
        int curZomboID = zombo.SceneObject.ID;
        if (displayedZombos_ID.IndexOf(curZomboID) != -1)
            return;

        GameObject newZombosPanel = Instantiate(zomboChoicePanelPrefab, content.transform);
        displayedZombos.Add(newZombosPanel);
        displayedZombos_ID.Add(curZomboID);

        ZomboPanel zomboPanel = newZombosPanel.GetComponent<ZomboPanel>();
        zomboPanel.clip.sequence = "zomb_" + curZomboID + "_idle_0";
        displayedZombos_panels.Add(zomboPanel);

        Button upgradeButton = zomboPanel.upgradeButton;
        upgradeButton.onClick.AddListener(delegate{ this.ZomboChoice(curZomboID); });
    }

    public void ZomboChoice(int zomboID)
    {
        if(PlayersDataKeeper.BoughtenZombos
            .Find(zmb => zmb.SceneObject.ID == zomboID) == null)
            zomboID = 0;

        // Unselecting last Zombo
        foreach(ZomboPanel panel in displayedZombos_panels)
            panel.platform.sprite = platforms[0];
                    
        // Selecting current Zombo
        PlayersDataKeeper.SelectedZomboID = zomboID;
        int zomboIndex = displayedZombos_ID.IndexOf(zomboID);
        displayedZombos_panels[zomboIndex].platform.sprite = platforms[1];
    }
}
