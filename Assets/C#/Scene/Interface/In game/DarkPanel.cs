﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class DarkPanel : MonoBehaviour
{
    [SerializeField] private float duration = 0.5f;
    private Image image;
    private float oneTimeStep;
    private Color curColor;
    private float timeFromStart;

    void Awake()
    {
        image = GetComponent<Image>();
        curColor = image.color;
        curColor.a = 0;
        image.color = curColor;
    }

    void Update()
    {
        if(timeFromStart >= duration)
            Destroy(this);

        curColor.a = EasingEquations.EaseOutSine(timeFromStart, 0, 1, duration);
        image.color = curColor;

        timeFromStart += Time.deltaTime;
    }
}
