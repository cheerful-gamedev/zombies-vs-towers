﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseWindow : MonoBehaviour
{
    void Start()
    {
        // For analytic
        if(StaticKeeper.Instance.PlayersDataKeeper.SelectedPlanetID == -1)
            StaticKeeper.Instance.AnalyticObject.Lose();
    }

    public void RestartLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void RestartLevelWithBonus()
    {
        StaticKeeper.Instance.PlayersDataKeeper.KEnergy = 1.2f;
        Application.LoadLevel(Application.loadedLevel);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
