﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using FTRuntime;

public class WinWindow : MonoBehaviour
{
    [SerializeField] private int showOnFrame;
    [SerializeField] private GameObject allLabels;
    [SerializeField] MoneyCalculation _coins, _diamonds;
    [SerializeField] private TextMesh
        coinsText,
        diamondsText,
        towersText,
        zombosText;

    [SerializeField] private GameObject[] rewardedButtons;
    [SerializeField] private SwfClip clip;
    [SerializeField] private SwfClipController clipController;
    [SerializeField] private DiscoveringContent DiscoveringContent;

    private int coins, diamonds;

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private GameDataKeeper GameDataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        GameDataKeeper = StaticKeeper.GameDataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        #endregion
    }

    IEnumerator Start()
    {
        float planetCoefficient = ShopDataKeeper.Planets.Find(planet => planet.ID == PlayersDataKeeper.SelectedPlanetID).MoneyCoefficient;
        
        //For analitic
        if(PlayersDataKeeper.SelectedPlanetID == -1)
            planetCoefficient = StaticKeeper.AnalyticObject.analyticPlanet.MoneyCoefficient;
        
        coins = Mathf.RoundToInt(
            (GameDataKeeper.PointsForDestruction + GameDataKeeper.spawnedZombos)
            + GameDataKeeper.arrivedZombos * 3f
            + GameDataKeeper.Energy * 3f
            * planetCoefficient
        );
        coinsText.text = "0";

        diamonds = coins / 30;
        diamondsText.text = "0";

        zombosText.text = GameDataKeeper.spawnedZombos.ToString();
        towersText.text = GameDataKeeper.destroyedTowers.ToString(); 
        
        yield return new WaitForSeconds(showOnFrame/30); // 30 fps animation
        allLabels.SetActive(true);

        // Приминяем коэф. валют
        coins = Mathf.RoundToInt(coins * PlayersDataKeeper.KCoin);
        diamonds = Mathf.RoundToInt(diamonds * PlayersDataKeeper.KDiamond);
        
        // Изменяем текст с помощью "анимации"
        _coins.AmountCalculation(0, coins);
        _diamonds.AmountCalculation(0, diamonds);

        // For analytic
        if(StaticKeeper.Instance.PlayersDataKeeper.SelectedPlanetID == -1)
            StaticKeeper.Instance.AnalyticObject.Win(coins, diamonds);

        yield return new WaitForSeconds(.3f);

        foreach(GameObject button in rewardedButtons)
            button.SetActive(true);
    }

    public void CoinsWereChosen()
    {
        clip.sequence = "chose_coin";
        clipController.Play(true);
        PlayersDataKeeper.Coin += int.Parse(coinsText.text);
        coinsText.transform.parent.GetComponent<Button>().interactable = false;
        diamondsText.transform.parent.GetComponent<Button>().interactable = false;
        Invoke("CheckToGetRquiredWins", (float)clip.frameCount/30f);
    }

    public void DiamondsWereChosen()
    {
        clip.sequence = "chose_diamond";
        clipController.Play(true);
        PlayersDataKeeper.Diamond += int.Parse(diamondsText.text);
        coinsText.transform.parent.GetComponent<Button>().interactable = false;
        diamondsText.transform.parent.GetComponent<Button>().interactable = false;
        Invoke("CheckToGetRquiredWins", (float)clip.frameCount/30f);
    }

    private void CheckToGetRquiredWins()
    {
        gameObject.SetActive(false);

        int currentPlanetIndex = PlayersDataKeeper.SelectedPlanetID;
        PlanetObject curPlanet = ShopDataKeeper.Planets.Find(pl => pl.ID == currentPlanetIndex);
        curPlanet.CurrentWins++;
        StaticKeeper.Instance.UpdatingTheAvailableCategory();

        if(curPlanet.CurrentWins >= curPlanet.RequiredWins)
        {
            #region Open next Planet and Zombo
            PlanetObject nextPlanet = ShopDataKeeper.Planets.Find(pl => pl.ID == currentPlanetIndex+1);
            if(nextPlanet == null)
                LoadScene();
            else
            {
                if(nextPlanet.Available == false)
                {
                    StaticKeeper.Instance.UpdateAllData();
                    DiscoveringContent.Show();
                }
                else
                {
                    JustInCase();
                    LoadScene();
                }
            }
            #endregion
        }
        else
            LoadScene();
    }

    public void LoadScene()
    {
        StaticKeeper.Instance.UpdateAllData();
        SceneManager.LoadScene("Menu");
    }

    void JustInCase()
    {
        int currentPlanetIndex = PlayersDataKeeper.SelectedPlanetID;
        PlanetObject curPlanet = ShopDataKeeper.Planets.Find(pl => pl.ID == currentPlanetIndex && pl.Available == true);
        if(curPlanet == null)
            return;

        List<int> opensPlanetsID = new List<int>();
        List<int> opensZombosID = new List<int>();

        curPlanet.OpensPlanets.Split(new char[]{','}, System.StringSplitOptions.RemoveEmptyEntries).ToList()
            .ForEach(str => opensPlanetsID.Add(int.Parse(str)));

        curPlanet.OpensZombos.Split(new char[]{','}, System.StringSplitOptions.RemoveEmptyEntries).ToList()
            .ForEach(str => opensZombosID.Add(int.Parse(str)));

        opensPlanetsID.ForEach(planetID => 
            ShopDataKeeper.Planets.FindAll(pl => pl.ID == planetID)
                .ForEach(pl => pl.Available = true));
        opensZombosID.ForEach(zomboID => 
            ShopDataKeeper.Zombos.FindAll(zmb => zmb.SceneObject.ID == zomboID)
                .ForEach(zmb => zmb.Available = true));
    }

    public void DoubleCoins()
    {
        DeactiveRewardedButtons();
        _coins.AmountCalculation(coins, coins * 2);
    }

    public void DoubleDiamonds()
    {
        DeactiveRewardedButtons();
        _diamonds.AmountCalculation(diamonds, diamonds * 2);
    }

    public void BothCurrencyChosen()
    {
        DeactiveRewardedButtons();
        // clip.sequence = "chose_both";
        // clipController.Play(true);
        PlayersDataKeeper.Coin += int.Parse(coinsText.text);
        PlayersDataKeeper.Diamond += int.Parse(diamondsText.text);
        coinsText.transform.parent.GetComponent<Button>().interactable = false;
        diamondsText.transform.parent.GetComponent<Button>().interactable = false;
        Invoke("CheckToGetRquiredWins", 0);
    }

    private void DeactiveRewardedButtons()
    {
        foreach(GameObject button in rewardedButtons)
            Destroy(button);
    }
}
