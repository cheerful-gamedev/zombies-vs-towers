﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndController : MonoBehaviour
{
    private GameManager gameManager;
    private bool win;

    private void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private IEnumerator OnTriggerEnter(Collider collider)
    {
        if(!win && collider.tag.Equals("Zombo"))
        {
            win = true;
            GameDataKeeper GDK = StaticKeeper.Instance.GameDataKeeper;
            GDK.arrivedZombos = GDK.SpawnedZombos.Count;
            yield return StartCoroutine(gameManager.Win());
            Destroy(this);
        }
    }
}
