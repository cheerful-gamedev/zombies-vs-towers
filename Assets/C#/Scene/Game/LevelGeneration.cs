﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.AI;

public class LevelGeneration : MonoBehaviour
{
    [SerializeField] private Tilemap map;
    [SerializeField] private GameObject endPrefab;
    [SerializeField] private SpriteRenderer backgroundRenderer;

    [Header ("Road options")]
    [SerializeField] private int oneLineLenght = 5; // Длина одного длока дороги
    [SerializeField] private int roadWidth = 1; // Ширина одного блока дороги
    [SerializeField] private int roadItereaion = 5; // Кол-во блоков дороги
    [SerializeField] private TileBase baseHexagon; // Плитка
    [SerializeField] private TileBase endHexagon; // Плитка для отоброжения конца карты
    [SerializeField] private NavMeshSurface nav; // Ссылка на NavMeshSurface

    [Header ("Parents (Classes)")]
    [SerializeField] private Transform towersParent;

    [Header("Debuging")]
    [SerializeField] private TileBase maskHexagon; // Debuging hexagon
    [SerializeField] private int maskDepth = 1; // Mask size
    [SerializeField] private Tilemap debugMap; // Tilemap for debuging

    private List<int> availableTowers = new List<int>();
    private List<int> availableTraps = new List<int>();

    // Hidden variables
    private List<Vector3Int> mainRoadLine = new List<Vector3Int>(); // Координаты всех плиток главной линии дороги
    private List<Vector3Int> allCells = new List<Vector3Int>(); // Координаты всех плиток дороги
    private Vector3Int curPosition;
    private float damageCoefficient;

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private DataKeeper DataKeeper;
    private GameDataKeeper GameDataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        DataKeeper = StaticKeeper.DataKeeper;
        GameDataKeeper = StaticKeeper.GameDataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        #endregion
        
        TileBase baseHexagon_cache = baseHexagon;

        GetPlanetSettingFromPLayerDataKeeper();

        if(baseHexagon_cache != baseHexagon)
            map.SwapTile(baseHexagon_cache, baseHexagon);
    }

    private void GetPlanetSettingFromPLayerDataKeeper()
    {
        int planetID = PlayersDataKeeper.SelectedPlanetID;
        PlanetObject curPlanet = ShopDataKeeper.Planets.Find(planet => planet.ID == planetID);

        // For analitic
        if(PlayersDataKeeper.SelectedPlanetID == -1) // Get planet char-s for test
            curPlanet = StaticKeeper.AnalyticObject.analyticPlanet;

        oneLineLenght = curPlanet.OneLineLenght;
        roadWidth = curPlanet.RoadWidth;
        roadItereaion = curPlanet.RoadItereaion;
        baseHexagon = curPlanet.BaseHexagon;
        maskDepth = curPlanet.MaskDepth;
        backgroundRenderer.sprite = curPlanet.Background;
        damageCoefficient = curPlanet.TowerDamageCoefficient;

        curPlanet.AvailableTowers.Split(new char[]{','}, System.StringSplitOptions.RemoveEmptyEntries).ToList()
            .ForEach(str => availableTowers.Add(int.Parse(str)));

        curPlanet.AvailableTraps.Split(new char[]{','}, System.StringSplitOptions.RemoveEmptyEntries).ToList()
            .ForEach(str => availableTraps.Add(int.Parse(str)));
    }

    public void StartSpawn()
    {
        InvokeRepeating("Spawn", 0, .1f);        
    }

    // Recursive method
    void Spawn()
    {
        mainRoadLine.Clear();
        Vector3Int vector = new Vector3Int(1, 0, 0);
        
        #region Create main line of the Road
        for(int i=0; i<oneLineLenght; i++)
        {
            map.SetTile(curPosition, baseHexagon);
            mainRoadLine.Add(curPosition);
            allCells.Add(curPosition);
            curPosition += vector;
        }
        #endregion
        
        #region Create additional blocks for main with roadWidth parameter
        Vector3Int redraw = new Vector3Int(0, 1, 0);
        int maxIteraions = (roadWidth - 1) / 2 + 1;
        for (int i=1; i<maxIteraions; i++)
        {
            Vector3Int curPosition = Vector3Int.zero;
            mainRoadLine.ForEach(position => {
                // Up spawn
                curPosition = position + new Vector3Int(0, i, 0);
                map.SetTile(curPosition, baseHexagon);
                if(i!=1 && i!=maxIteraions-1)
                    allCells.Add(curPosition);


                // if(debugMap.GetTile(curPosition) != maskHexagon && i!=1 && i!=maxIteraions-1 && Random.Range(0,10) < 1)
                //     SpawnTowerAtPosition(curPosition);

                // Down spawn
                curPosition = position + new Vector3Int(0, -i, 0);
                map.SetTile(curPosition, baseHexagon);
                if(i != 1 && i != maxIteraions-1)
                    allCells.Add(curPosition);

                // if(debugMap.GetTile(curPosition) != maskHexagon && i != 1 && i != maxIteraions-1 && Random.Range(0,10) < 1)
                //     SpawnTowerAtPosition(curPosition);
            });
        }
        #endregion

        #region Tower spawning
        allCells.ForEach(position => {
            if(debugMap.GetTile(position) != maskHexagon && Random.Range(0,10) == 7)
                SpawnTowerAtPosition(position);
        });
        #endregion

        #region Recursive controller
        roadItereaion--;
        if(roadItereaion <= 0)
        {
            StopGeneration();
            // SpawnTowers();
        }
        #endregion
    }

    // Spawn one tower at position
    void SpawnTowerAtPosition(Vector3Int position)
    {
        int towerIndex = availableTowers[Random.Range(0, availableTowers.Count)];
        SceneObject SO_tower = DataKeeper.Towers[towerIndex];
        // Spawn
        GameObject tower = Instantiate(
            SO_tower.Prefab,
            map.GetCellCenterWorld(position),
            Quaternion.identity
        );
        // Change parent
        tower.transform.parent = towersParent;

        TowerInfo TI = tower.GetComponent<TowerInfo>();
        // Create new instance of SceneObject
        TI.Initialization(SO_tower, damageCoefficient);
        // Add new Tower to GameDataKeeper
        GameDataKeeper.SpawnedTowers.Add(TI.Info);

        // Cosmetic
        DrawBuildingZone(position, debugMap);
    }

    void DrawBuildingZone(Vector3Int position, Tilemap map, int iterationNumber = 0)
    {
        List<Vector3Int> positionAround = new List<Vector3Int>();
        // map.SetTile(position, maskHexagon);

        positionAround.Add(position + new Vector3Int(0, 1, 0)); // Right
        positionAround.Add(position + new Vector3Int(0, -1, 0)); // Left
        positionAround.Add(position + new Vector3Int(1, 0, 0)); // Up
        positionAround.Add(position + new Vector3Int(-1, 0, 0)); // Down

        if(position.y % 2 == 0)
        {
            positionAround.Add(position + new Vector3Int(-1, 1, 0)); // Left Up
            positionAround.Add(position + new Vector3Int(-1, -1, 0)); // Left Down
        }
        else
        {
            positionAround.Add(position + new Vector3Int(1, 1, 0)); // Right UP
            positionAround.Add(position + new Vector3Int(1, -1, 0)); // Right Down
        }

        positionAround.ForEach(pos => map.SetTile(pos, maskHexagon));

        if(maskDepth > 1 && iterationNumber < maskDepth)
            positionAround.ForEach(pos => 
                DrawBuildingZone(
                    pos, 
                    map, 
                    (iterationNumber == 0) ? 2 : iterationNumber++
                ));

        // Debug.Log(iterationNumber + " " +positionAround.Count);
    }

    // U know
    void StopGeneration()
    {
        CancelInvoke();

        #region Spawn additionaly road for End object
            mainRoadLine.Clear();
            Vector3Int vector = new Vector3Int(1, 0, 0);

            #region Create main line of the Road
            for (int i = 0; i < 7; i++)
            {
                map.SetTile(curPosition, baseHexagon);
                mainRoadLine.Add(curPosition);
                curPosition += vector;
            }
            #endregion

            #region Create additional blocks for main with roadWidth parameter
            Vector3Int redraw = new Vector3Int(0, 1, 0);
            for (int i = 1; i < (roadWidth - 1) / 2 + 1; i++)
            {
                Vector3Int curPosition = Vector3Int.zero;
                mainRoadLine.ForEach(position => {
                    // Up spawn
                    curPosition = position + new Vector3Int(0, i, 0);
                    map.SetTile(curPosition, baseHexagon);

                    // Down spawn
                    curPosition = position + new Vector3Int(0, -i, 0);
                    map.SetTile(curPosition, baseHexagon);
                });
            }
            #endregion
        #endregion

        Vector3 endPosition = map.GetCellCenterWorld(mainRoadLine[mainRoadLine.Count - 4]);
        endPosition.z = 0;
        GameObject endObj = Instantiate(endPrefab, endPosition, Quaternion.identity);
        GameDataKeeper.End = endObj.transform;

        // Spawn Collider for NavMesh
        BoxCollider collider = map.gameObject.AddComponent<BoxCollider>();
        collider.center = new Vector3(collider.center.x, 0, 0);
        collider.size = new Vector3(collider.size.x, Mathf.RoundToInt(roadWidth/2), 0.5f);
        nav.BuildNavMesh();
        SendMessage("LevelGenerationWasEnded");
        Destroy(this);
    }
}
