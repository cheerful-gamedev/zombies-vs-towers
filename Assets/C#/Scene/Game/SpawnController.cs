﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField] private Transform cave;
    [SerializeField] private ZomboChoiceContent ZCC;

    #region All ScriptableObjects
    private DataKeeper DataKeeper;
    private GameDataKeeper GameDataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        DataKeeper = StaticKeeper.Instance.DataKeeper;
        GameDataKeeper = StaticKeeper.Instance.GameDataKeeper;
        ShopDataKeeper = StaticKeeper.Instance.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.Instance.PlayersDataKeeper;
        #endregion
    }

    public void RequestForSpawn()
    {
        // For analityc (visualization of selecting)
        if(PlayersDataKeeper.SelectedPlanetID == -1)
            ZCC.ZomboChoice(PlayersDataKeeper.SelectedZomboID);

        SceneObject SO_zombo = GameDataKeeper.ZombosCache.Find(zmb => zmb.ID == PlayersDataKeeper.SelectedZomboID);

        if(GameDataKeeper.Energy >= SO_zombo.Energy)
        {
            // Refresh the eneregy and energy bar
            GameDataKeeper.Energy -= SO_zombo.Energy;
            SendMessage("RefreshEnergyBar", (float)GameDataKeeper.Energy/GameDataKeeper.StartEnergy);

            // Spawn
            Vector3 spawnPosition = cave.position;
            spawnPosition.y += Random.Range(-2.5f, 0f);
            spawnPosition.x += Random.Range(-1.2f, 0.3f);
            GameObject zombo = Instantiate(SO_zombo.Prefab, spawnPosition, Quaternion.identity);

            ZomboInfo ZI = zombo.GetComponent<ZomboInfo>();
            // Create new instance of SceneObject
            ZI.Initialization(SO_zombo);
            // Add new Zombo to GameDataKeeper
            GameDataKeeper.SpawnedZombos.Add(ZI.Info);

            // Set destionation for Zombo
            switch (SO_zombo.TargetType)
            {
                case TargetType.Tower:
                    ZI.Info.TargetType = TargetType.Tower;
                    zombo.SendMessage("SetNewTarget", GameDataKeeper.Tower);
                    break;

                case TargetType.End:
                    ZI.Info.TargetType = TargetType.End;
                    zombo.SendMessage("SetNewTarget", GameDataKeeper.End);
                    break;
            }

            GameDataKeeper.spawnedZombos++;
        }
        else if(GameDataKeeper.Energy > 0)// Если игрок жмет, а у него не хватает энергии
        {
            int id = PlayersDataKeeper.SelectedZomboID;
            id = id-1 >= 0 ? id-1 : 0;
            ZCC.ZomboChoice(id);
            RequestForSpawn();
        }
    }
}
