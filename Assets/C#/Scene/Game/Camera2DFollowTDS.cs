﻿using System.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;


[RequireComponent(typeof(Camera))]
public class Camera2DFollowTDS : MonoBehaviour {

	[SerializeField] private float smooth = 2.5f; // Сглаживание при следовании за персонажем
	[SerializeField] private float thresholdX = 0.4f, thresholdY = 0.4f; // "Слепая" зона в центре
	[SerializeField] private float offset; // Значение смещения (отключить = 0)
	[SerializeField] private float offsetY; // Отступ по Y
	[SerializeField] private Tilemap map; // TileMap, в рамках которого будет перемещаться камера
	[SerializeField] private bool useBounds = true; // Использовать или нет, границы для камеры

	private GameDataKeeper GameDataKeeper;
	private float curTimeWithoutMoving = 2, maxTimeWithoutMoving = 2; // Текущее время без движения камеры и максимально допустимое
	private Vector3 min, max, direction;
	private static Camera2DFollowTDS _use;
	private Camera cam;

	public static Camera2DFollowTDS use
	{
		get{ return _use; }
	}

	void Awake()
	{
		GameDataKeeper = StaticKeeper.Instance.GameDataKeeper;

		_use = this;
		cam = GetComponent<Camera>();
		cam.orthographic = true;
		CalculateBounds();
		offset = Screen.width*2;

        // Перерасчет дистанции от центра экрана (для границ)
        thresholdX *= (float)Screen.width / 1200f;
        thresholdY *= (float)Screen.height / 600f;
    }

	// переключатель, для использования из другого класса
	public void UseCameraBounds(bool value)
	{
		useBounds = value;
	}

	// если в процессе игры, было изменено разрешение экрана
	// или параметр "Orthographic Size", то следует сделать вызов данной функции повторно
	public void CalculateBounds()
	{
		if(map == null) return;
		// Bounds mapBounds = map.localBounds;
		map.CompressBounds();
		// mapBounds = new Bounds(mapBounds.center, new Vector3(map.localBounds.size.x, mapBounds.size.y, mapBounds.size.z));
		Bounds bounds = Camera2DBounds();
		min = bounds.max + map.localBounds.min;
		max = bounds.min + map.localBounds.max;
	}

	Bounds Camera2DBounds()
	{
		float height = cam.orthographicSize;
		return new Bounds(Vector3.zero, new Vector3(height * cam.aspect * 2, height, 0));
	}

	Vector3 MoveInside(Vector3 current, Vector3 pMin, Vector3 pMax)
	{
		if(!useBounds || map == null) return current;
		current = Vector3.Max(current, pMin);
		current = Vector3.Min(current, pMax);
		return current;
	}

	Vector3 Mouse()
	{
		Vector3 mouse = Input.mousePosition;
		mouse.z = -transform.position.z;
		return cam.ScreenToWorldPoint(mouse);
	}

	void Follow(Transform target = null)
	{
        if (target != null) // Цель - Зомбо
        {
            direction = (target.position - Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, transform.position.z))).normalized;
        }
        else // Цель - точка на экране
        {
            direction = (Mouse() - Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, transform.position.z))).normalized;
            // Если нажатие было в "слепой" зоне
            if (Mathf.Abs(direction.x) < thresholdX && Mathf.Abs(direction.y) < thresholdY)
                return;

            curTimeWithoutMoving = 0;
        }

		Vector3 position = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width / 2, Screen.height / 2, transform.position.z)).normalized + direction * offset;
		position.z = transform.position.z;
		position.y = transform.position.y;
		position = MoveInside(position, new Vector3(min.x, min.y, position.z), new Vector3(max.x, max.y, position.z));
		transform.position = Vector3.Lerp(transform.position, position, Mathf.Abs(direction.x) * smooth * Time.fixedDeltaTime);
	}

	void LateUpdate()
	{
		// Если нажатия по экрану не было давно
		if(curTimeWithoutMoving >= maxTimeWithoutMoving)
			Follow(GameDataKeeper.FarthestZombo());
		// Если есть нажатие на экран
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetMouseButton(0))
			Follow();
		else
			curTimeWithoutMoving += Time.fixedDeltaTime;
	}
}