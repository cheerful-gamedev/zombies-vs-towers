﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpawnController))]
public class InputController : MonoBehaviour
{
    [SerializeField] private float thresholdX = 0.4f, thresholdY = 0.4f; // "Слепая" зона в центре
    private SpawnController SpawnController;
    private float neededDelay = .1f, curDelay;
    
    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private DataKeeper DataKeeper;
    private GameDataKeeper GameDataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        DataKeeper = StaticKeeper.DataKeeper;
        GameDataKeeper = StaticKeeper.GameDataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        #endregion

        SpawnController = GetComponent<SpawnController>();

        // Перерасчет дистанции от центра экрана (для границ)
        thresholdX *= (float)Screen.width / 1200f;
        thresholdY *= (float)Screen.height / 600f;

        #if UNITY_EDITOR
            if(PlayersDataKeeper.SelectedPlanetID == -1)
            {

            }
        #endif
    }

    void Update()
    {

#if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.anyKey && curDelay >= neededDelay)   
        {
            Vector3 direction = (Mouse() - Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, transform.position.z))).normalized;
            // Debug.Log(Mathf.Abs(direction.x) + " / " + thresholdX + "\n" + Mathf.Abs(direction.y) + " / " + thresholdY);
            if (Mathf.Abs(direction.x) < thresholdX && Mathf.Abs(direction.y) < thresholdY)
            {
                curDelay = 0;
                for(int i=0; i<PlayersDataKeeper.KSpawnerSpeed; i++)
                    SpawnController.RequestForSpawn();
            }
        }
        // For analytic
        else if(PlayersDataKeeper.SelectedPlanetID == -1 && curDelay >= neededDelay)
        {
            curDelay = 0;
            for(int i=0; i<PlayersDataKeeper.KSpawnerSpeed; i++)
            {
                PlayersDataKeeper.SelectedZomboID = StaticKeeper.AnalyticObject.ChooseZomboID();
                SpawnController.RequestForSpawn();
            }
        }
        else
        {
            curDelay += Time.deltaTime;
        }
#else
        if(Input.touchCount > 0 && curDelay >= neededDelay)
        {
            Vector3 direction = (Mouse() - Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, transform.position.z))).normalized;
            if (Mathf.Abs(direction.x) < thresholdX && Mathf.Abs(direction.y) < thresholdY)
            {
                curDelay = 0;
                for(int i=0; i<PlayersDataKeeper.KSpawnerSpeed; i++)
                    SpawnController.RequestForSpawn();
            }
        }
        else
        {
            curDelay += Time.deltaTime;
        }
#endif
    }

    Vector3 Mouse()
    {
        Vector3 mouse = Input.mousePosition;
        mouse.z = -transform.position.z;
        return Camera.main.ScreenToWorldPoint(mouse);
    }
}
