﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerController : MonoBehaviour
{
    [SerializeField] private int sortingOrderBase = 100;
    [SerializeField] private float refreshDelay = .1f;
    [SerializeField] private int offset = 0, zPos = 10;
    [SerializeField] private bool itIsStatic;
    [SerializeField] private Renderer myRenderer;
    [SerializeField] private ObjectType type;

    private enum ObjectType{ Tower, Zombo, Wall, Bullet }

    private float curRefreshDelay;

    void Start()
    {
        if(type == ObjectType.Tower || type == ObjectType.Wall)
            transform.position = new Vector3(transform.position.x, transform.position.y, zPos);
    }

    void LateUpdate()
    {
        curRefreshDelay -= Time.fixedDeltaTime;
        if(curRefreshDelay <= 0)
        {
            myRenderer.sortingOrder = (int)(sortingOrderBase - transform.position.y*100 - offset);
            if(itIsStatic)
                Destroy(this);
            curRefreshDelay = refreshDelay;
        }
    }
}
