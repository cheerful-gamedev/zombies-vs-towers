﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SceneObjectSoundsSystem : MonoBehaviour
{
    [SerializeField] private AudioSource source;
    [SerializeField] private List<AudioClip> kicks = new List<AudioClip>();
    [SerializeField] private List<AudioClip> dies = new List<AudioClip>();
    private PlayersDataKeeper PDK;
    
    void Awake()
    {
        PDK = StaticKeeper.Instance.PlayersDataKeeper;
    }

    public void PlayKickSound() 
    {
        if(!PDK.EffectsOn) return; // Если эффекты выключены
        
        AudioClip sound = kicks[Random.Range(0, kicks.Count)];
        PlaySound(sound);
    }

    public void PlayDieSound() 
    {
        if(!PDK.EffectsOn) return; // Если эффекты выключены

        AudioClip sound = dies[Random.Range(0, dies.Count)];
        PlaySound(sound);
    }

    private void PlaySound(AudioClip sound)
    {
        if(!PDK.EffectsOn) return; // Если эффекты выключены

        source.clip = sound;
        source.Play();
    }
}
