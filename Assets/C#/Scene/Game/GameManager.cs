﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [RequireComponent(typeof(InputController))]
// [RequireComponent(typeof(SpawnController))]
public class GameManager : MonoBehaviour
{
    [Header("Post-fight windows")]
    [SerializeField] private GameObject winWindow;
    [SerializeField] private GameObject loseWindow;
    [SerializeField] private GameObject darkPanel;

    private InputController InputController;
    private float mapLenght; // Distance between Cave and End 
    private float lastTraveledDistance;
    private bool win, lose;

    #region All ScriptableObjects
    private StaticKeeper StaticKeeper;
    private DataKeeper DataKeeper;
    private GameDataKeeper GameDataKeeper;
    private ShopDataKeeper ShopDataKeeper;
    private PlayersDataKeeper PlayersDataKeeper;
    #endregion

    void Awake()
    {
        #region Initialize all ScriptableObjects
        StaticKeeper = StaticKeeper.Instance;
        DataKeeper = StaticKeeper.DataKeeper;
        GameDataKeeper = StaticKeeper.GameDataKeeper;
        ShopDataKeeper = StaticKeeper.ShopDataKeeper;
        PlayersDataKeeper = StaticKeeper.PlayersDataKeeper;
        #endregion
    
        ResetAllTimelessVariables();
        InputController = GetComponent<InputController>();

        UpgradeZombosCache();
    }

    void Start()
    {
        GameDataKeeper.Cave = GameObject.Find("Cave").transform;
        GameDataKeeper.PointsForDestruction = 0;
        SendMessage("StartSpawn");
    }

    void FixedUpdate()
    {
        if(!win && GameDataKeeper.SpawnedZombos.Count == 0 && GameDataKeeper.Energy <= 0 && !lose)
            StartCoroutine(Lose());

        try
        { 
            // Refresh MapComlited Bar
            float traveledDistance = Vector2.Distance(GameDataKeeper.Cave.position, GameDataKeeper.FarthestZombo().position);
            if (traveledDistance > 0 && lastTraveledDistance < traveledDistance)
            {
                lastTraveledDistance = traveledDistance;
                SendMessage("RefreshBar", traveledDistance / mapLenght);
            }
        }
        catch { }
    }

    // When all parts of road was generated
    public void LevelGenerationWasEnded()
    {
        GameDataKeeper.Tower = GameDataKeeper.NearestTowerObject();
        InputController.enabled = true;
        GameObject.Find("Main Camera").GetComponent<Camera2DFollowTDS>().CalculateBounds();
        mapLenght = Vector2.Distance(GameDataKeeper.Cave.position, GameDataKeeper.End.position) - 1.5f;
    }

    // Reset all for new iteration of game
    void ResetAllTimelessVariables()
    {
        GameDataKeeper.Energy = Mathf.RoundToInt(PlayersDataKeeper.Energy * PlayersDataKeeper.KEnergy);
        GameDataKeeper.StartEnergy = GameDataKeeper.Energy;
        PlayersDataKeeper.KEnergy = 1;
        GameDataKeeper.SpawnedTowers.Clear();
        GameDataKeeper.SpawnedZombos.Clear();
        GameDataKeeper.ZombosCache.Clear();
        GameDataKeeper.TowersCache.Clear();
        GameDataKeeper.Tower = null;
        GameDataKeeper.Zombo = null;
        GameDataKeeper.PointsForDestruction = 0;
        GameDataKeeper.destroyedTowers = 0;
        GameDataKeeper.spawnedZombos = 0;

        if(PlayersDataKeeper.SelectedPlanetID != -1
            && PlayersDataKeeper.BoughtenZombos
                .Find(zmb => zmb.SceneObject.ID == PlayersDataKeeper.SelectedZomboID) == null)
            PlayersDataKeeper.SelectedZomboID = 0;
        else if(PlayersDataKeeper.SelectedPlanetID == -1)
            Time.timeScale = StaticKeeper.Instance.AnalyticObject.iterationSpeed;
    }

    // If player winned
    public IEnumerator Win()
    {
        win = true;
        Debug.Log("Win");

        Destroy(GetComponent<InputController>());
        Destroy(GetComponent<SpawnController>());

        // new List<SceneObject>(GameDataKeeper.SpawnedZombos).ForEach(zombo => zombo.Object.SendMessage("Die"));
        foreach (SceneObject SO in new List<SceneObject>(GameDataKeeper.SpawnedZombos))
            SO.Object.SendMessage("Die", SendMessageOptions.DontRequireReceiver);

        yield return new WaitForSeconds(1f);
        darkPanel.SetActive(true);
        yield return new WaitForSeconds(.5f);

        winWindow.SetActive(true); 
        StaticKeeper.SoundSystem.PlaySound("buy");
    }

    // If player losed
    public IEnumerator Lose()
    {
        lose = true;

        Destroy(GetComponent<InputController>());
        Destroy(GetComponent<SpawnController>());

        yield return new WaitForSeconds(.5f);
        darkPanel.SetActive(true);
        yield return new WaitForSeconds(.5f);

        Debug.Log("Lose");
        loseWindow.SetActive(true);
        StaticKeeper.SoundSystem.PlaySound("buy error");
    }

    void UpgradeZombosCache()
    {
        System.Type type = System.Type.GetType("SceneObject");

        // For all boughten Zombos
        List<ShopObject> zombos = new List<ShopObject>();
        if(PlayersDataKeeper.SelectedPlanetID == -1)
            zombos = ShopDataKeeper.Zombos.FindAll(zmb => zombos.IndexOf(zmb) == -1);
        else
            zombos = PlayersDataKeeper.BoughtenZombos;
        
        foreach(ShopObject zombo in zombos)
        {
            int curZomboID = zombo.SceneObject.ID;

            // Creating new instance of SceneObject for cache
            SceneObject zomboFromDataKeeper = DataKeeper.Zombos.Find(zmb => zmb.ID == curZomboID);
            SceneObject zomboForCache = ScriptableObject.CreateInstance<SceneObject>();
            zomboForCache.UpdateAllData(zomboFromDataKeeper);
            GameDataKeeper.ZombosCache.Add(zomboForCache);
            
            // Adding all Zombo's upgrades
            try{ PlayersDataKeeper.BoughtenUpgrades.RemoveAll(null); } catch{}
            List<UpgradeObject> upgradesForZombo = PlayersDataKeeper.BoughtenUpgrades
                .FindAll(up => up.UpgradeTargetType == UpgradeTargetType.Zombo && up.ID == curZomboID);
            
            // For all upgrades, which were found
            foreach(UpgradeObject upgrade in upgradesForZombo)
            {
                // Getting
                float newValue = (float)type.GetMethod("get_"+upgrade.CharacteristicType.ToString())
                    .Invoke(zomboForCache, null);

                // Multiplying
                newValue *= upgrade.IncreasedValue;

                // Setting
                type.GetMethod("set_"+upgrade.CharacteristicType.ToString())
                    .Invoke(zomboForCache, new object[]{newValue});
            }
        }
    }
}
