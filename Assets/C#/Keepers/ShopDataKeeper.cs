﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="ShopDataKeeper", menuName="Keepers/ShopDataKeeper")]
[System.Serializable]
public class ShopDataKeeper : ScriptableObject
{
    [Header("Zombos")]
    public List<ShopObject> Zombos = new List<ShopObject>();

    [Header("Planets")]
    public List<PlanetObject> Planets = new List<PlanetObject>();

    [Header("Zombo Upgrades")]
    public List<UpgradeObject> Upgrades = new List<UpgradeObject>();

    [Header("Player Upgrades")]
    public List<UpgradeObject> PlayerUpgrades = new List<UpgradeObject>();

    [Header("Purchases")]
    public List<PurchaseObject> Purchases = new List<PurchaseObject>();
}
