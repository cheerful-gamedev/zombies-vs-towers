﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="DataKeeper", menuName="Keepers/DataKeeper")]
[System.Serializable]
public class DataKeeper : ScriptableObject
{
    #region Variables for safe
    [Header("Zombos")]
    [SerializeField] private List<SceneObject> zombos = new List<SceneObject>();
    [Header("Towers")]
    [SerializeField] private List<SceneObject> towers = new List<SceneObject>();
    #endregion    
    
    #region Calling methods for getting and setting
    public List<SceneObject> Zombos{get { return zombos;} set{zombos = value;}}
    public List<SceneObject> Towers{get { return towers;} set{towers = value;}}
    #endregion
}
