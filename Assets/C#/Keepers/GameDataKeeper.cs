﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/* GameDataKeeper - File for safe all data which needed to game iteration */
[CreateAssetMenu(fileName="GameDataKeeper", menuName="Keepers/GameDataKeeper")]
public class GameDataKeeper : ScriptableObject
{
    [Header("Player's data")]
    public int Energy;
    public int StartEnergy;
    
    [Header("NavMesh's targets")]
    public Transform Tower;
    public Transform Zombo;
    public Transform End;

    [Header("Zombos")]
    public List<SceneObject> ZombosCache = new List<SceneObject>();
    public List<SceneObject> SpawnedZombos = new List<SceneObject>();

    [Header("Towers")]
    public List<SceneObject> TowersCache = new List<SceneObject>();
    public List<SceneObject> SpawnedTowers = new List<SceneObject>();
    public Transform Cave;

    public int PointsForDestruction;
    public int destroyedTowers, spawnedZombos, arrivedZombos;

    // Get nearest Tower
    public Transform NearestTowerObject()
    {
        float nearestSqrMag = float.PositiveInfinity;
        SceneObject nearestTower = null;
        SpawnedTowers.ForEach(tower => {
            float sqrMag = (Cave.position - tower.Object.transform.position).sqrMagnitude;
            if(sqrMag < nearestSqrMag)
            {
                nearestSqrMag = sqrMag;
                nearestTower = tower;
            }
        });

        if (nearestTower == null || SpawnedTowers.Count == 0)
        {
            foreach (SceneObject SO in SpawnedZombos.FindAll(zombo => zombo.TargetType == TargetType.Tower))
            {
                SO.TargetType = TargetType.End;
                SO.Object.SendMessage("SetNewTarget", End);
            }
            return null;
        }
        else
            return nearestTower.Object.transform;
    }

    // The farthest Zombo
    public SceneObject FarthestZomboGetObject()
    {
        float nearestSqrMag = float.NegativeInfinity;
        SceneObject farthestZombo = null;
        SpawnedZombos.ForEach(zombo => {
            float sqrMag = (Cave.position - zombo.Object.transform.position).sqrMagnitude;
            if (sqrMag > nearestSqrMag)
            {
                nearestSqrMag = sqrMag;
                farthestZombo = zombo;
            }
        });

        if (farthestZombo == null || SpawnedZombos.Count == 0)
            return null;
        else
            return farthestZombo;
    }

    // The farthest Zombo
    public Transform FarthestZombo()
    {
        SceneObject farthestZombo = FarthestZomboGetObject();

        if (farthestZombo == null || SpawnedZombos.Count == 0)
            return null;
        else
            return farthestZombo.Object.transform;
    }

    // The nex position of farthest Zombo
    public Vector3 FarthestZomboGetFuturePosition(Vector3 from, float bulletSpeed)
    {
        SceneObject farthestZombo = FarthestZomboGetObject();

        if (farthestZombo == null || SpawnedZombos.Count == 0)
            return Cave.position;
        else
        {
            NavMeshAgent agent = farthestZombo.Object.GetComponent<NavMeshAgent>();
            float distance = Vector2.Distance(from, agent.transform.position);
            float time = distance / bulletSpeed;
            float anticipation = time * agent.speed * Mathf.Sin(Vector2.Angle(from, agent.transform.position));
            NavMeshHit hit;
            if (agent.SamplePathPosition(NavMesh.AllAreas, anticipation, out hit))
                return hit.position;
            else
                return farthestZombo.Object.transform.position;
        }
    }
}
