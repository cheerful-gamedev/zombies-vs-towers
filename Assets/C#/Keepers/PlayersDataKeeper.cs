﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="PlayersDataKeeper", menuName="Keepers/PlayersDataKeeper")]
[System.Serializable]
public class PlayersDataKeeper : ScriptableObject
{
    #region Variables for safe
    [SerializeField] private int energy;
    [SerializeField] private int selectedZomboID;
    [SerializeField] private int selectedPlanetID;
    [SerializeField] private int coin;
    [SerializeField] private int diamond;
    [SerializeField] private float kCoin = 1; // Коэф. получения монет
    [SerializeField] private float kDiamond = 1; // Коэф. получения алмазов
    [SerializeField] private float kSpawnerSpeed = 1; // Коэф. увеличения скорости
    [SerializeField] private float kEnergy = 1; // Коэф. увеличения энергии

    [Header("Purchases")]
    [SerializeField] private List<ShopObject> boughtenZombos = new List<ShopObject>();
    [SerializeField] private List<UpgradeObject> boughtenUpgrades = new List<UpgradeObject>();
    [SerializeField] private List<UpgradeObject> boughtenPlayerUpgrades = new List<UpgradeObject>();
    [SerializeField] private List<PlanetObject> boughtenPlanets = new List<PlanetObject>();

    [Header("Available")]
    [SerializeField] private List<int> availableZombos = new List<int>(new int[]{0, 1});
    [SerializeField] private List<int> availablePlanets = new List<int>(new int[]{0});
    [SerializeField] private List<int> currentWinsOnPlanet = new List<int>(new int[]{0});

    [Header("Settings")]
    [SerializeField] private bool musicOn = true;
    [SerializeField] private bool effectsOn = true;
    [SerializeField] private int flagIndex = 15; // Индекс флага в настройках
    #endregion

    #region Calling methods for getting and setting
    public int Energy{get { return energy;} set{energy = value;}}
    public int SelectedZomboID{get { return selectedZomboID;} set{selectedZomboID = value;}}
    public int SelectedPlanetID{get { return selectedPlanetID;} set{selectedPlanetID = value;}}
    public int Coin{get { return coin;} set{coin = value;}}
    public int Diamond{get { return diamond;} set{diamond = value;}}
    public float KCoin{get { return kCoin;} set{kCoin = value;}}
    public float KDiamond{get { return kDiamond;} set{kDiamond = value;}}
    public float KSpawnerSpeed{get { return kSpawnerSpeed;} set{kSpawnerSpeed = value;}}
    public float KEnergy{get { return kEnergy;} set{kEnergy = value;}}
    public List<ShopObject> BoughtenZombos{get { return boughtenZombos;} set{boughtenZombos = value;}}
    public List<UpgradeObject> BoughtenUpgrades { get { return boughtenUpgrades; } set{boughtenUpgrades = value;} }
    public List<UpgradeObject> BoughtenPlayerUpgrades { get { return boughtenPlayerUpgrades; } set{boughtenPlayerUpgrades = value;} }
    public List<PlanetObject> BoughtenPlanets { get { return boughtenPlanets; } set{boughtenPlanets = value;} }

    public List<int> AvailableZombos { get { return availableZombos; } set{availableZombos = value;} }
    public List<int> AvailablePlanets { get { return availablePlanets; } set{availablePlanets = value;} }
    public List<int> CurrentWinsOnPlanet { get { return currentWinsOnPlanet; } set{currentWinsOnPlanet = value;} }

    public bool MusicOn { get { return musicOn; } set{musicOn = value;} }
    public bool EffectsOn { get { return effectsOn; } set{effectsOn = value;} }
    public int FlagIndex { get { return flagIndex; } set{flagIndex = value;} }


    #endregion
}
