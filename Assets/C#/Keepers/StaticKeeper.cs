﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public class StaticKeeper : Singleton<StaticKeeper> 
{
    public DataKeeper DataKeeper;
    public GameDataKeeper GameDataKeeper;
    public ShopDataKeeper ShopDataKeeper;
    public PlayersDataKeeper PlayersDataKeeper;
    public AnalyticObject AnalyticObject;
    public SoundSystem SoundSystem;

    private string folderName = "YbVelmsBaKS4hhUj";
    private string fileName = "EHpoHirs0d7v4LWR.funtick";

    void Awake()
    {
        // DontDestroyOnLoad(this);
        DataKeeper = Resources.Load<DataKeeper>("DataKeeper");
        GameDataKeeper = Resources.Load<GameDataKeeper>("GameDataKeeper");
        ShopDataKeeper = Resources.Load<ShopDataKeeper>("ShopDataKeeper");
        PlayersDataKeeper = Resources.Load<PlayersDataKeeper>("PlayersDataKeeper");
        AnalyticObject = Resources.Load<AnalyticObject>("Analytic Object");
    
        ReadFromDataFile();
        CheckBoughtenZombos();
        CheckBoughtenPlanets();
    }
    
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SoundSystem = GameObject.Find("Sound System").GetComponent<SoundSystem>();
    }

    public bool CheckPlayerData()
    {
        string fullPath = Path.Combine(Application.persistentDataPath, folderName);
        if(!File.Exists(Path.Combine(fullPath, fileName))) // Файл отсутствует
        {
            Directory.CreateDirectory(fullPath);
            return false;
        }
        else // Файл уже есть
            return true;
    }

    public void ReadFromDataFile()
    {
        if(!CheckPlayerData())
            WriteToDataFile();

        string directory = Path.Combine(Application.persistentDataPath, folderName); // Путь к бэкапу класс-файла
        string rowData = File.ReadAllText(Path.Combine(directory, fileName));
        string afterDecrypt = Encrypter.EncryptDecrypt(rowData);
        // Debug.Log(afterDecrypt);
        JsonUtility.FromJsonOverwrite(afterDecrypt, PlayersDataKeeper);
    }

    public void WriteToDataFile()
    {
        CheckPlayerData();
        string directory = Path.Combine(Application.persistentDataPath, folderName); // Путь к бэкапу класс-файла
        PlayersDataKeeper data = PlayersDataKeeper as PlayersDataKeeper;
        // Debug.Log(JsonUtility.ToJson(data));
        string afterEncrypt = Encrypter.EncryptDecrypt(JsonUtility.ToJson(data));
        File.WriteAllText(Path.Combine(directory, fileName), afterEncrypt);
    }

    public void UpdateAllData()
    {
        #region Трекаем коэф. на наличие нечестной игры (изменения значений без покупки)
        // Coins
        UpgradeObject kCoin = PlayersDataKeeper.BoughtenPlayerUpgrades.Find(u => u.CharacteristicType == CharacteristicType.Coin);
        if(kCoin == null)
        {
            PlayersDataKeeper.KCoin = 1;
        }
        // Diamonds
        UpgradeObject kDiamond = PlayersDataKeeper.BoughtenPlayerUpgrades.Find(u => u.CharacteristicType == CharacteristicType.Diamond);
        if(kDiamond == null)
        {
            PlayersDataKeeper.KDiamond = 1;
        }
        // SpawnerSpeed
        UpgradeObject kSpawnerSpeed = PlayersDataKeeper.BoughtenPlayerUpgrades.Find(u => u.CharacteristicType == CharacteristicType.SpawnerSpeed);
        if(kSpawnerSpeed == null)
        {
            PlayersDataKeeper.KSpawnerSpeed = 1;
        }
        #endregion

        UpdatingFromTheAvailableCategory();
        UpdatingTheAvailableCategory();
        WriteToDataFile();
    }

    public void UpdatingTheAvailableCategory()
    {
        PlayersDataKeeper.AvailableZombos.Clear();
        ShopDataKeeper.Zombos
            .FindAll(zmb => zmb.Available == true)
            .ForEach(zmb => {
                int zomboID = zmb.SceneObject.ID;
                if(PlayersDataKeeper.AvailableZombos.IndexOf(zomboID) == -1)
                    PlayersDataKeeper.AvailableZombos.Add(zomboID);
            });

        PlayersDataKeeper.AvailablePlanets.Clear();
        PlayersDataKeeper.CurrentWinsOnPlanet.Clear();
        ShopDataKeeper.Planets
            .FindAll(planet => planet.Available == true)
            .ForEach(planet => {
                PlayersDataKeeper.AvailablePlanets.Add(planet.ID);
                PlayersDataKeeper.CurrentWinsOnPlanet.Add(planet.CurrentWins);
            });

        PlayersDataKeeper.BoughtenPlanets.Clear();
        ShopDataKeeper.Planets
            .FindAll(planet => planet.Boughten)
            .ForEach(planet => PlayersDataKeeper.BoughtenPlanets.Add(planet));
    }

    public void UpdatingFromTheAvailableCategory()
    {
        PlayersDataKeeper.AvailableZombos.ForEach(zomboID => {
            ShopDataKeeper.Zombos
                .FindAll(zmb => zmb.SceneObject.ID == zomboID)
                .ForEach(zmb => zmb.Available = true);
        });

        PlayersDataKeeper.AvailablePlanets.ForEach(planetID => {
            ShopDataKeeper.Planets
                .FindAll(planet => planet.ID == planetID)
                .ForEach(planet => {
                    planet.Available = true;
                    int planetLocalID = PlayersDataKeeper.AvailablePlanets.IndexOf(planetID);
                    planet.CurrentWins = PlayersDataKeeper.CurrentWinsOnPlanet[planetLocalID];
                });
        });

        PlayersDataKeeper.BoughtenPlanets.ForEach(planet => planet.Boughten = true);
    }

    // Проверка купленных Зомбо
    public void CheckBoughtenZombos()
    {
        PlayersDataKeeper.BoughtenZombos = PlayersDataKeeper.BoughtenZombos.Where(z => z != null).ToList();

        // All free Zombos
        foreach (ShopObject zombo in ShopDataKeeper.Zombos.FindAll(z => z.ObjectType == ObjectType.Zombo && z.Price == 0))
            if(PlayersDataKeeper.BoughtenZombos.Find(obj => obj.SceneObject.ID == zombo.SceneObject.ID) == null)
                PlayersDataKeeper.BoughtenZombos.Add(zombo);
    }

    // Проверка купленных планет
    public void CheckBoughtenPlanets()
    {
        PlayersDataKeeper.BoughtenPlanets = PlayersDataKeeper.BoughtenPlanets.Where(z => z != null).ToList();

        // All free Planets
        foreach (PlanetObject planet in ShopDataKeeper.Planets.FindAll(z => z.Price == 0 || z.Boughten))
            if(PlayersDataKeeper.BoughtenPlanets.Find(obj => obj.ID == planet.ID) == null)
                PlayersDataKeeper.BoughtenPlanets.Add(planet);
    }
}
